

#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "ExchangeSimulatorRequestHandlerFactory.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CommonRequestHandlerFactory.h"
#include "UnknownRequestHandler.h"
#include "EntityToModuleStateStats.h"

ExchangeSimulatorRequestHandlerFactory::ExchangeSimulatorRequestHandlerFactory(EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}

Poco::Net::HTTPRequestHandler* ExchangeSimulatorRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request) {

        auto commonRequestHandler = commonRequestHandlerFactory->createRequestHandler(request);
        if (commonRequestHandler != nullptr) {
                return commonRequestHandler;
        }

        entityToModuleStateStats->addStateModuleForEntity("UNKNOWN_REQUEST", "ExchangeSimulatorRequestHandlerFactory", request.getURI());
        return new UnknownRequestHandler(entityToModuleStateStats);
}
