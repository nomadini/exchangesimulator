#include <cstdlib>
#include <exception>      // std::set_terminate

#include <chrono>
#include <thread>
#include "SignalHandler.h"
#include "OpenRtbBidRequest.h"
#include "BidderLoadTester.h"
#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequestParser.h"
#include "StackTraceUtil.h"
#include "RandomUtilDecimalGenerator.h"
#include <stack>
#include "DateTimeUtil.h"
#include "ActionRecorderLoadTester.h"
#include "BidderLoadTester.h"
#include "AdServerLoadTester.h"
#include "MySqlDriver.h"
#include "ExchangeIdService.h"
#include "AdServerCaller.h"
#include "EntityToModuleStateStats.h"
#include "RandomUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "EntityToModuleStateStats.h"
#include "RandomUtil.h"
#include "HttpUtil.h"
#include "Device.h"
#include "ConfigService.h"
#include "CollectionUtil.h"


AdServerCaller::AdServerCaller() {
        goodAdServerResponse =  std::make_shared<gicapods::AtomicLong>();
        sleeInMicroSecondsBetweenEachBidderCall =  std::make_shared<gicapods::AtomicLong>();
        syncedGoogleId =  std::make_shared<gicapods::AtomicLong>();
        priceRandomizer = std::make_shared<RandomUtilDecimalGenerator> (1, 2);
}

void AdServerCaller::run() {


        while (true) {
                LOG_EVERY_N(ERROR, 30)<<"bidderResponses->size() "<<bidderResponses->size();

                while(!bidderResponses->empty()) {

                        auto bidResponse = bidderResponses->dequeueEntity();
                        if (bidResponse == boost::none) {
                                continue;
                        }
                        callAdServer(**bidResponse);
                        // gicapods::Util::sleepMillis(1);
                }
                gicapods::Util::sleepViaBoost(_L_, 1);
        }

}

void AdServerCaller::callAdServer(std::string bidResponse) {
        if(bidResponse.empty()) {
                entityToModuleStateStats->
                addStateModuleForEntity("EMPTY_BID_RESPONSE_FROM_BIDDER",
                                        "AdServerCaller",
                                        "ALL");
                return;
        }

        MLOG(3)<<"bid response from bidder : "<<bidResponse;
        try {
                entityToModuleStateStats->
                addStateModuleForEntity("NON_EMPTY_BID_RESPONSE_FROM_BIDDER",
                                        "AdServerCaller",
                                        "ALL",
                                        EntityToModuleStateStats::important);
                std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> bidResponsePtr =
                        OpenRtb2_3_0::OpenRtbBidResponseJsonConverter::fromJson(bidResponse);

                if (bidResponsePtr != nullptr) {

                        std::string nurl = bidResponsePtr->seatbid->bid->nurl;
                        auto notificationUrl =
                                StringUtil::replaceStringCaseInsensitive(nurl, "${AUCTION_PRICE}",
                                                                         StringUtil::toStr(bidResponsePtr->seatbid->bid->price +
                                                                                           priceRandomizer->randomDecimalV2()));

                        // notificationUrl = "http://localhost:9981/win?won=3.552052080603878&trnId=ey";

                        std::string adserverResponse
                                = adServerLoadTester->sendWinNotificationsToAdserv(notificationUrl);

                        callMatchTag(adserverResponse);
                        callClickUrl(adserverResponse);
                        if(adserverResponse.empty()) {
                                LOG_EVERY_N(ERROR, 1000) << google::COUNTER<< "th empty adserv response";
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "EMPTY_AD_RESPONSE_FROM_ADSERVER",
                                        "AdServerCaller",
                                        EntityToModuleStateStats::all
                                        );

                                return;
                        }

                        goodAdServerResponse->increment();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "GOOD_AD_RESPONSE_FROM_ADSERVER",
                                "AdServerCaller",
                                EntityToModuleStateStats::all
                                );

                        MLOG(3) << "nurl : "<< nurl<< ", "<<std::endl<<
                                "notificationUrl : "<< notificationUrl;
                        MLOG(3) << "goodAdServerResponse from adserver : "
                                << goodAdServerResponse->getValue();

                        MLOG(3)<<"adserver Response : "<< adserverResponse;
                } else {
                        LOG(WARNING)<<"bad bidder Response "<< bidResponse;
                }

        } catch(...) {
                LOG(WARNING)<< "exception caught";
                entityToModuleStateStats->addStateModuleForEntity(
                        "EXCEPTION",
                        "AdServerCaller",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        }
}

void AdServerCaller::callClickUrl(std::string adserverResponse) {
        MLOG(3) << "adserverResponse : "<< adserverResponse;
        // auto links = gumboHtmlParser->extractLinks(adserverResponse);
        // MLOG(3) << "links : " << JsonArrayUtil::convertListToJson(links);
        // LOG(ERROR) << "links : " << JsonArrayUtil::convertListToJson(links);

        //root@alpha1:/home/vagrant/workspace/mango# php artisan serve
        //now we create a file this this
        std::vector<std::string> links = StringUtil::searchWithRegex(adserverResponse, "href=.*>");
        // LOG(ERROR) << "links : " << JsonArrayUtil::convertListToJson(links);
        // assertAndThrow(links.size()> 0);
        // auto link = links.at(0);
        // LOG(ERROR) << "link : " << link;
}

void AdServerCaller::callMatchTag(std::string adserverResponse) {
        //
        //<img src="http://cm.g.doubleclick.net/pixel?google_nid=1234&google_cm" />
        std::string googlePixelMatchTag = "<img src=\"http://cm.g.doubleclick.net/pixel?google_nid=1234&google_cm\" />";
        if (StringUtil::contains(adserverResponse, googlePixelMatchTag)) {
                //refer to this to see how pixel matching works
                //https://developers.google.com/ad-exchange/rtb/cookie-guide
                std::string firstPart = "googleId";
                std::string googleId = StringUtil::random_string( firstPart, 12);
                std::string newGoogleId = StringUtil::replaceString(googlePixelMatchTag,
                                                                    "google_nid=1234",
                                                                    "google_nid=" + googleId);
                //TODO : read the properties from actionrecorder.properties
                std::string actionRecorderUrl = "http://localhost:9989/googlepixelmatching?google_gid=dGhpcyBpcyBhbiBleGFtGxl&google_cver=1";

                auto response = actionRecorderLoadTester->session->sendRequest(
                        *PocoHttpRequest::createSimplePostRequest(
                                actionRecorderUrl,
                                ""));

                syncedGoogleId->increment();
                entityToModuleStateStats->addStateModuleForEntity(
                        "syncedGoogleId",
                        "AdServerCaller",
                        EntityToModuleStateStats::all);
                // allSyncedGoogleIds.insert(newGoogleId);
                tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor;

                std::string nomadiniDeviceIdCookie =
                        response->getRequiredCookieValue(HttpUtil::nomadiniDeviceId);

                //we create a device from the cookie to make sure
                //the cookie is set correctly
                auto device = std::make_shared<Device>(nomadiniDeviceIdCookie, "");
        }
}
