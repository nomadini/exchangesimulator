#ifndef AdServerLoadTester_H
#define AdServerLoadTester_H

#include "Poco/URI.h"
namespace OpenRtb2_3_0 { class OpenRtbBidRequest; }
namespace gicapods { class ConfigService; }

#include <memory>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "BidderLoadTester.h"
#include "ExchangeBeanFactory.h"
#include "PocoSession.h"
#include <tbb/concurrent_hash_map.h>

class RandomUtilDecimalGenerator;
class NomadiniSessionInterface;
class BidRequestEnricher;
class ExchangeIdService;
class ExchangeBeanFactory;
class MySqlPixelService;
class EventLog;

class AdServerLoadTester : public std::enable_shared_from_this<AdServerLoadTester> {

public:

Poco::URI loadTesterUri;
std::shared_ptr<PocoSession> session;
std::shared_ptr<RandomUtilDecimalGenerator> priceRandomizer;
gicapods::ConfigService* configService;
ExchangeBeanFactory* exchangeBeanFactory;
AdServerLoadTester(gicapods::ConfigService* configService,
                   EntityToModuleStateStats* entityToModuleStateStats);

void setUp();

BidderLoadTester* bidderLoadTester;

void startLoadTestingAdServer();

void startLoadTestingImpTracker(std::shared_ptr<EventLog> eventLog);
void startLoadTestingImpressionHanlder(std::shared_ptr<EventLog> eventLog);
void startLoadTestingClickTracker(std::shared_ptr<EventLog> eventLog);
std::shared_ptr<std::vector<std::shared_ptr<EventLog> > > prepareEvents(std::string eventLogInJson);
std::string sendWinNotificationsToAdserv(std::string pathAndQuery);

void callConversionPixel(std::shared_ptr<EventLog> eventLog);

};





#endif
