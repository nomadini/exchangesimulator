#include <cstdlib>
#include <exception>      // std::set_terminate

#include <chrono>
#include <thread>
#include "SignalHandler.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequestParser.h"
#include "StackTraceUtil.h"
#include "Poco/Runnable.h"
#include <stack>
#include "Poco/Util/PropertyFileConfiguration.h"

class MySqlDriver;
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include <set>
#include "BidRequestEnricher.h"
namespace gicapods { class ConfigService; }
#include "Poco/Net/HTTPResponse.h"
#include "AtomicLong.h"
#include "ConcurrentQueueFolly.h"

class EntityToModuleStateStats;
class BidderLoadTester;
class ExchangeIdService;
class AdServerLoadTester;
class ActionRecorderLoadTester;
class RandomUtilDecimalGenerator;

class AdServerCaller : public Poco::Runnable {
public:

std::shared_ptr<ConcurrentQueueFolly<std::string> > bidderResponses;
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
AdServerLoadTester* adServerLoadTester;
ActionRecorderLoadTester* actionRecorderLoadTester;
std::shared_ptr<gicapods::AtomicLong> sleeInMicroSecondsBetweenEachBidderCall;
std::shared_ptr<gicapods::AtomicLong> goodAdServerResponse;
std::shared_ptr<gicapods::AtomicLong> syncedGoogleId;
std::shared_ptr<RandomUtilDecimalGenerator> priceRandomizer;
AdServerCaller();


virtual void run();

void callAdServer(std::string bidResponse);

void callClickUrl(std::string adserverResponse);

void callMatchTag(std::string adserverResponse);
};
