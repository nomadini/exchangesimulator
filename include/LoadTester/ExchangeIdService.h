#ifndef ExchangeIdService_H
#define ExchangeIdService_H



#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include <set>
#include "AtomicLong.h"


class MySqlCreativeService;
class EntityToModuleStateStats;
class HttpUtilService;
class GicapodsIdToExchangeIdsMapCassandraService;
class ExchangeIdService;

class ExchangeIdService {

public:
int numberOfSyncedIdsToRead;
std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > > > exchangeToSyncedIdsReadFromMysql;
GicapodsIdToExchangeIdsMapCassandraService* gicapodsIdToExchangeIdsMapCassandraService;
ExchangeIdService();
virtual ~ExchangeIdService();
void setup();

std::string getRandomeSyncedBuyerId(std::string exchangeName);
std::string getRandomeSyncedNomadiniDeviceId(std::string exchangeName);
void refreshSyncedIdsFromMySql(bool continuous = true);

void insertPairOfIdsIntoOutgoingMap(
        std::string exchangeName, std::string nomadiniDeviceIdCookie, std::string googleId );
};

#endif
