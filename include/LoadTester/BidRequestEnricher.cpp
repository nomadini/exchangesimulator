//
// Created by Mahmoud Taabodi on 2/12/16.
//

#include "BidRequestEnricher.h"
#include "RandomUtil.h"
#include "User.h"
#include "UserAgentTestHelper.h"
#include "MySqlBWListService.h"
#include "MySqlTargetGroupService.h"
#include "HttpUtilService.h"
#include "EntityToModuleStateStats.h"
#include "StringUtil.h"
#include "TargetGroupBWListCacheService.h"
#include "MySqlGlobalWhiteListService.h"
#include "ExchangeIdService.h"
#include "MySqlGlobalWhiteListService.h"
#include "OpenRtbBidRequest.h"
#include "Site.h"
#include "FileUtil.h"
#include "SiteTestHelper.h"
#include "GlobalWhiteListEntry.h"


BidRequestEnricher::BidRequestEnricher(MySqlDriver* mySqlDriver,
                                       MySqlGlobalWhiteListService* mySqlGlobalWhiteListService,
                                       TargetGroupBWListCacheService* targetGroupBWListCacheService) {
        this->mySqlGlobalWhiteListService = mySqlGlobalWhiteListService;
        this->targetGroupBWListCacheService = targetGroupBWListCacheService;
        allWhiteListedEntries = mySqlGlobalWhiteListService->readAllEntities();

        //we want to log all whitelisted domains in a file, for testing
        if (!FileUtil::checkIfFileExists("/tmp/all-whitelisted-domains.txt")) {
                for(auto domain : allWhiteListedEntries) {
                        FileUtil::appendALineToFile("/tmp/all-whitelisted-domains.txt",
                                                    domain->getDomainName()+"\n");
                }
        }


        for(int i=0; i<20; i++) {
                std::shared_ptr<OpenRtb2_3_0::Banner> banner1 = std::make_shared<OpenRtb2_3_0::Banner>();
                banner1->w = 320; banner1->h=50;

                std::shared_ptr<OpenRtb2_3_0::Banner> banner2 = std::make_shared<OpenRtb2_3_0::Banner>();
                banner2->w = 300; banner2->h=50;

                std::shared_ptr<OpenRtb2_3_0::Banner> banner3 = std::make_shared<OpenRtb2_3_0::Banner>();
                banner3->w = 320; banner3->h=90;

                banners.push_back(banner1);
                banners.push_back(banner2);
                banners.push_back(banner3);
        }

        std::shared_ptr<OpenRtb2_3_0::Banner> bannerUnMatchSize = std::make_shared<OpenRtb2_3_0::Banner>();
        bannerUnMatchSize->w = 160; bannerUnMatchSize->h=600;
        banners.push_back(bannerUnMatchSize);
}

std::string BidRequestEnricher::getAWhiteListedGlobalDomain() {

        assertAndThrow(!allWhiteListedEntries.empty());

        auto randIndex = RandomUtil::sudoRandomNumber(allWhiteListedEntries.size() -1);
        return allWhiteListedEntries.at(randIndex)->getDomainName();
}

std::string BidRequestEnricher::getARandomBadDomain() {

        assertAndThrow(!allWhiteListedEntries.empty());
        auto randIndex = RandomUtil::sudoRandomNumber(allWhiteListedEntries.size() -1);
        auto goodDomain = allWhiteListedEntries.at(randIndex)->getDomainName();
        goodDomain.append("-bad");
        return goodDomain;
}

std::string BidRequestEnricher::getRandomCreativeAdType() {
        std::vector<std::string> adTypes = {"XHTML_TEXT_AD",  "XHTML_BANNER_AD", "JAVASCRIPT", "IFRAME"};
        auto randIndex = RandomUtil::sudoRandomNumber(adTypes.size() -1);
        return adTypes.at(randIndex);
}

//valid device types are defined in Device.h and they are from 1 to 7
int BidRequestEnricher::getRandomDeviceType() {
        auto randIndex = RandomUtil::sudoRandomNumber(6);
        return randIndex + 1;
}


std::string BidRequestEnricher::getRandomCreativeAPI() {
        std::vector<std::string> apis = {"VPAID1.0", "VPAID2.0", "MRAID-1", "ORMMA", "MRAID-2"};
        auto randIndex = RandomUtil::sudoRandomNumber(apis.size() -1);
        return apis.at(randIndex);
}


std::shared_ptr<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > > BidRequestEnricher::getKnownSites() {
        static std::shared_ptr<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > > known =
                std::make_shared<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > >();
        if (known->empty()) {
                for (int i=0; i< 100; i++) {
                        known->push_back(
                                SiteTestHelper::createOrGetSite(
                                        SiteTestHelper::getRandomIABCategory(),
                                        SiteTestHelper::getRandomDomain()));
                }

        }
        return known;
}

std::shared_ptr<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > > BidRequestEnricher::getUnKnownSites() {
        static std::shared_ptr<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > > unknown = std::make_shared<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > >();
        if (unknown->empty()) {
                for (int i=0; i< 1000; i++) {
                        unknown->push_back(SiteTestHelper::createOrGetSite(
                                                   SiteTestHelper::getRandomIABCategory(),
                                                   SiteTestHelper::getRandomDomain()));
                }

        }
        return unknown;
}

void BidRequestEnricher::setWhiteListedSite(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        auto size = targetGroupBWListCacheService->getWhiteListedDomainToListOfTargetGroups()->size();
        if (size <= 0) {
                LOG_EVERY_N(ERROR, 1000)<< "size of whiteListedDomainToListOfTargetGroups is zero..fix this";
                return;
        }
        auto random = RandomUtil::sudoRandomNumber(size);
        if (random > 1) {
                if (targetGroupBWListCacheService->getWhiteListedDomainsAssignedToTargetGroups ()->empty()) {
                        MLOG(2) <<"there is no white list assigned ";
                        return;
                }
                auto whiteList = targetGroupBWListCacheService->getWhiteListedDomainsAssignedToTargetGroups ()->at(random - 1);
                bidRequest->site = SiteTestHelper::createOrGetSite (SiteTestHelper::getRandomIABCategory (),
                                                                    whiteList);
        }
}

void BidRequestEnricher::setBlackListedSite(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        auto size = targetGroupBWListCacheService->getWhiteListedDomainToListOfTargetGroups()->size();
        auto random = RandomUtil::sudoRandomNumber(size);
        if (random >= 1) {
                if (targetGroupBWListCacheService->getBlackListedDomainsAssignedToTargetGroups ()->empty()) {
                        VLOG(0) <<"there is no black list assigned ";
                        return;
                }
                bidRequest->site = SiteTestHelper::createOrGetSite (SiteTestHelper::getRandomIABCategory (),
                                                                    targetGroupBWListCacheService->
                                                                    getBlackListedDomainsAssignedToTargetGroups ()->at
                                                                            (random - 1));
        } else {
                MLOG(2) << "random is less than 1 : " << random;
        }

}

void BidRequestEnricher::setKnownBuyerId(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest,
                                         std::string exchangeName) {

        bidRequest->deviceOpenRtb->ua = UserAgentTestHelper::getRandomUserAgentFromFiniteSet();
        bidRequest->user->buyeruid = exchangeIdService->getRandomeSyncedBuyerId(exchangeName);
}

void BidRequestEnricher::setNoBuyerId(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        bidRequest->user->buyeruid = "";
}

std::shared_ptr<OpenRtb2_3_0::Banner> BidRequestEnricher::getRandomBanner() {

        auto randIndex = RandomUtil::sudoRandomNumber(banners.size() -1);
        return banners.at(randIndex);
}

void BidRequestEnricher::setRandomCreativeSize(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        auto banner = getRandomBanner();

        bidRequest->imp->banner->w = banner->w;
        bidRequest->imp->banner->h = banner->h;
}

void BidRequestEnricher::setBlockedAdvertiser(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        auto sites = getKnownSites();
        auto randIndex = RandomUtil::sudoRandomNumber(sites->size() -1);

        bidRequest->badv.push_back(sites->at(randIndex)->domain);
}
