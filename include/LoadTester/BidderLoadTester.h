#ifndef BidderLoadTester_H
#define BidderLoadTester_H

#include "OpenRtbBidRequest.h"
#include "Poco/URI.h"
#include "OpenRtbBidRequest.h"
namespace gicapods { class ConfigService; }
#include "BidRequestEnricher.h"
#include <memory>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "ConcurrentQueueFolly.h"
#include "PocoSession.h"
#include "AtomicLong.h"
class BidRequestCreator;
class EntityToModuleStateStats;
class StatisticsUtil;

class BidderLoadTester : public std::enable_shared_from_this<BidderLoadTester> {

public:
std::shared_ptr<ConcurrentQueueFolly<std::string> > requestBodies;
std::shared_ptr<ConcurrentQueueFolly<std::string> > bidderResponses;

std::vector<std::shared_ptr<PocoSession> > allSessions;
std::shared_ptr<BidRequestCreator> bidRequestCreator;
std::shared_ptr<StatisticsUtil> statisticsUtil;
gicapods::ConfigService* configService;
BidRequestEnricher* bidRequestEnricher;
EntityToModuleStateStats* entityToModuleStateStats;
std::string exchangeName;
int numberOfRequests;

BidderLoadTester(std::string propertyFileName,
                 BidRequestEnricher* bidRequestEnricher,
                 gicapods::ConfigService* configService,
                 EntityToModuleStateStats* entityToModuleStateStats,
                 std::string exchangeName,
                 int numberOfRequests,
                 std::string hostUrl);


void sendRequestsToBidder();
void prepareRequests();
std::shared_ptr<PocoSession> getSession();

void createRealBlockingBRQ_Strategy(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);

void createSomeBlockingBRQ_Strategy(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);

void setABlockedWebSiteForBidRequest(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);

void callMeForDebugging();
void setConsistentIabCatsPerDevicePercentageGroup(
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);
std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> createARequest(std::string exchangeName);
};

#endif
