#include "GUtil.h"
#include "FileUtil.h"
#include "OpenRtbBidRequest.h"
#include "EntityToModuleStateStats.h"
#include "BidderLoadTester.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include <cmath>
#include "ConverterUtil.h"
#include "NetworkUtil.h"
#include "BidRequestEnricher.h"
#include <Util/StatisticsUtil.h>
#include "ConfigService.h"
#include "EntityToModuleStateStats.h"
#include "RandomUtil.h"
#include "SiteTestHelper.h"
#include "DeviceTestHelper.h"
#include "BidRequestCreator.h"
#include "PocoSession.h"
using namespace OpenRtb2_3_0;

BidderLoadTester::BidderLoadTester(std::string propertyFileName,
                                   BidRequestEnricher* bidRequestEnricher,
                                   gicapods::ConfigService* configService,
                                   EntityToModuleStateStats* entityToModuleStateStats,
                                   std::string exchangeName,
                                   int numberOfRequests,
                                   std::string hostUrl) {

        this->bidRequestEnricher = bidRequestEnricher;
        this->configService = configService;
        bidRequestCreator = std::make_shared<BidRequestCreator>();
        this->exchangeName = exchangeName;
        this->numberOfRequests = numberOfRequests;
        requestBodies = std::make_shared<ConcurrentQueueFolly<std::string> >();


        //performance comes down when i goes up
        for (int i=0; i < 1; i++) {
                auto pocoSessionBidderLoadTester = std::make_shared<PocoSession>(
                        "BidderLoadTester",
                        configService->getAsBooleanFromString("keepAliveMode"),
                        configService->getAsInt("keepAliveTimeoutInMillis"),
                        configService->getAsInt("timeoutInMillis"),
                        hostUrl,
                        entityToModuleStateStats);


                allSessions.push_back(pocoSessionBidderLoadTester);
        }

        statisticsUtil = std::make_shared<StatisticsUtil>();
}

void BidderLoadTester::prepareRequests() {
        for (int i = 0; i < numberOfRequests; i++) {
                auto requestBody = createARequest(exchangeName)->toJson ();
                auto requestBodyShared = std::make_shared<std::string>(requestBody);
                if (requestBodies->size() <= 500000)  {
                        requestBodies->enqueueEntity(requestBodyShared);
                } else {
                        requestBodies->clear();
                }

        }

        LOG_EVERY_N (INFO, 1) << google::COUNTER<<"th requestBodies->size() : " << requestBodies->size();

}

std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> BidderLoadTester::createARequest(std::string exchangeName) {
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest =
                bidRequestCreator->createOpenRtbBidRequest ();


        bidRequest->deviceOpenRtb->ip =  DeviceTestHelper::getRandomIpAddressFromFiniteSet();
        bidRequest->site->domain = bidRequestEnricher->getAWhiteListedGlobalDomain();
        if (statisticsUtil->happensWithProb(0.001)) {
                bidRequest->site->domain = bidRequestEnricher->getARandomBadDomain();
        }

        bidRequestEnricher->setBlockedAdvertiser(bidRequest);
        bidRequestEnricher->setRandomCreativeSize(bidRequest);
        bidRequestEnricher->setKnownBuyerId(bidRequest, exchangeName);


        //0.001 of requests come from unknown users
        if (statisticsUtil->happensWithProb(0.001)) {
                bidRequestEnricher->setNoBuyerId(bidRequest);
        }

        if (statisticsUtil->happensWithProb(0.2)) {

                bidRequestEnricher->setWhiteListedSite(bidRequest);
        }

        if (statisticsUtil->happensWithProb(0.2)) {
                // bidRequestEnricher->setBlackListedSite(bidRequest); //throws some exception for some reason
        }


        setConsistentIabCatsPerDevicePercentageGroup(bidRequest);


        return bidRequest;
}

/*
   if a deviceid hash is divisbale by 1 , it gets iab cat 1
   if a deviceid hash is divisbale by 2 , it gets iab cat 2
   and so on.
   we are doing this to make a group of devices have a certain IAB Cat and SUB CATs for clustering
   purposes
 */
void BidderLoadTester::setConsistentIabCatsPerDevicePercentageGroup(
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        long hashOfBidRq = StringUtil::getHashedNumber(bidRequest->user->buyeruid);
        int siteCat = std::abs(hashOfBidRq % 10  + 1);
        int siteSubCat = RandomUtil::sudoRandomNumber(10);
        std::string iabCategory1 =
                "IAB" + _toStr(siteCat);
        std::string iabCategory2 =
                "IAB" + _toStr(siteCat + 1);
        std::shared_ptr<Site> site =
                SiteTestHelper::createOrGetSite(iabCategory1,
                                                SiteTestHelper::getRandomDomain());
        //we add another category to the site for this device always
        //to make one device have 2 categories, in order to find the right clusters in
        //Clusterer
        site->cat.push_back(iabCategory2);
        site->publisher->cat.push_back(iabCategory2);


        bidRequest->site = site;
}

void BidderLoadTester::sendRequestsToBidder() {

        std::string bidResponse;


        std::string bidderFinalUrl ("__BIDDER_URL__/bid/__exchangeName__/openrtb2.3");

        bidderFinalUrl = StringUtil::replaceString (bidderFinalUrl, "__exchangeName__", exchangeName);
        bidderFinalUrl = StringUtil::replaceString (bidderFinalUrl, "__BIDDER_URL__",
                                                    getSession()->getServerUrl());


        auto numberOfSuccessfulRequestsSent = std::make_shared<gicapods::AtomicLong>();



        //this value shouldnt be that high, it blocks us from
        //calling adserver
        for(int i=0; i < 100; i++) {
                // while(requestBodies->empty()) {

                boost::optional<std::shared_ptr<std::string> > requestBody = requestBodies->dequeueEntity ();
                if (requestBody == boost::none) {
                        break;
                }
                MLOG(3)<< " hitting bidder with requestBody : "<< **requestBody;
                auto response = getSession()->sendRequest(
                        *PocoHttpRequest::createSimplePostRequest(
                                bidderFinalUrl,
                                **requestBody));
                bidderResponses->enqueueEntity(std::make_shared<std::string>(response->body));
                if (!response->body.empty()) {
                        MLOG(3)<<"good bid response from bidder : "<< response->body;
                }

                LOG_EVERY_N (INFO, 10000) << google::COUNTER<<"th hitting bidder at  : "
                                          << bidderFinalUrl
                                          << " with "<< numberOfRequests<< " requests";

        }

        entityToModuleStateStats->addStateModuleForEntityWithValue(
                "bid_request",
                requestBodies->size(),
                "BidderLoadTester",
                EntityToModuleStateStats::all
                );

        entityToModuleStateStats->addStateModuleForEntityWithValue(
                "successful_brq_sent",
                numberOfSuccessfulRequestsSent->getValue(),
                "BidderLoadTester",
                EntityToModuleStateStats::all
                );
}

void BidderLoadTester::createRealBlockingBRQ_Strategy(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        setABlockedWebSiteForBidRequest (bidRequest);
}

void BidderLoadTester::createSomeBlockingBRQ_Strategy(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {
        if (RandomUtil::sudoRandomNumber (80) > 20) {
                //in 25% of times we see that bid requests are from blocking websites
                setABlockedWebSiteForBidRequest (bidRequest);
        }
}

void BidderLoadTester::setABlockedWebSiteForBidRequest(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {

}

std::shared_ptr<PocoSession> BidderLoadTester::getSession() {

        return allSessions.at(0);

}
