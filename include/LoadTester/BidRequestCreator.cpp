#include "GUtil.h"
#include "FileUtil.h"
#include "OpenRtbBidRequest.h"
#include "CassandraDriverInterface.h"
#include "BidRequestCreator.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include "ConverterUtil.h"
#include "NetworkUtil.h"
#include "BidRequestEnricher.h"
#include <Util/StatisticsUtil.h>
#include "MySqlBWListService.h"
#include "ConfigService.h"
#include "DeviceTestHelper.h"
#include "UserAgentTestHelper.h"
#include "SiteTestHelper.h"
#include "OpenRtbBidRequest.h"
using namespace OpenRtb2_3_0;

BidRequestCreator::BidRequestCreator(){
        // # http://en.wikipedia.org/wiki/Extreme_points_of_the_United_States#Westernmost
        // top = 49.3457868 # north lat
        // left = -124.7844079 # west long
        // right = -66.9513812 # east long
        // bottom =  24.7433195 # south lat
        //boundaries of US lat
        usLatRandomizer = std::make_shared<RandomUtilDecimalGenerator>(24.7433195 + 1, 49.3457868 - 1);
        //boundaries of US lon
        usLonRandomizer = std::make_shared<RandomUtilDecimalGenerator>(-124.7844079 + 1, -66.9513812 - 1);

}

BidRequestCreator::~BidRequestCreator(){
}

std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> BidRequestCreator::createOpenRtbBidRequest() {

        std::shared_ptr<OpenRtbBidRequest> bidRequest = std::make_shared<OpenRtbBidRequest>();
        bidRequest->id = StringUtil::random_string(24);
        bidRequest->tmax = 100;


        for (int i=0; i < 3; i++) {
                auto randomBlockedCategory = SiteTestHelper::getRandomIABCategory();
                bidRequest->bcat.push_back(randomBlockedCategory);
        }
        // bidRequest->badv.push_back(randomBlockedCategory);

        std::shared_ptr<Impression> imp = std::make_shared<Impression>();
        imp->id = "imp" + StringUtil::random_string(12);

        imp->bidFloor = ConverterUtil::convertTo<double>(
                StringUtil::toStr("1.") + StringUtil::toStr(RandomUtil::sudoRandomNumber(100)));
        std::shared_ptr<Banner> banner = std::make_shared<Banner>();
        banner->w = 180;
        banner->h = 150;
        banner->api.push_back(RandomUtil::sudoRandomNumber(4) + 1);
        banner->pos = RandomUtil::sudoRandomNumber(7);
        banner->battr.push_back(1);
        imp->banner = banner;

        int randomNumber = RandomUtil::sudoRandomNumber(1000);
        auto randomCategory = SiteTestHelper::getRandomIABCategory();
        std::shared_ptr<Site> site = SiteTestHelper::createOrGetSite(
                SiteTestHelper::getRandomIABCategory(),
                SiteTestHelper::getRandomDomain());

        auto deviceOpenRtb = std::make_shared<DeviceOpenRtb>();
        deviceOpenRtb->ip = DeviceTestHelper::getRandomIpAddressFromFiniteSet();
        deviceOpenRtb->ua = UserAgentTestHelper::getRandomUserAgentFromFiniteSet();

        // Mobile_Tablet = 1,
        // Personal_Computer = 2,
        // Connected_TV = 3,
        // Phone = 4,
        //sending request for desktop
        deviceOpenRtb->deviceType = 2;

        MLOG(2) << "device->ua :  " << deviceOpenRtb->ua << " , device->ip : " << deviceOpenRtb->ip;


        deviceOpenRtb->geo->lat = usLatRandomizer->randomDecimalV2();
        deviceOpenRtb->geo->lon = usLonRandomizer->randomDecimalV2();

        deviceOpenRtb->geo->country = StringUtil::toStr("US");
        deviceOpenRtb->geo->city = StringUtil::toStr("NEW YORK");
        deviceOpenRtb->geo->region = StringUtil::toStr("NEW YORK");
        deviceOpenRtb->geo->zip = StringUtil::toStr("11102");

        std::shared_ptr<User> user = std::make_shared<User>();
        user->buyeruid = "user1000000000000";
        bidRequest->user = user;
        bidRequest->imp = imp;
        bidRequest->site = site;
        bidRequest->deviceOpenRtb = deviceOpenRtb;

        return bidRequest;

}
