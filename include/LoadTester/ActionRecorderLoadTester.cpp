#include "GUtil.h"
#include "FileUtil.h"
#include "OpenRtbBidRequest.h"
#include "CassandraDriverInterface.h"
#include "ActionRecorderLoadTester.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include "ConverterUtil.h"
#include "NetworkUtil.h"
#include "BidRequestEnricher.h"
#include <Util/StatisticsUtil.h>
#include "MySqlBWListService.h"
#include "ConfigService.h"
#include "NomadiniSessionInterface.h"
#include "RandomUtil.h"
#include "StringUtil.h"
#include "Pixel.h"
#include <thread>
#include "Device.h"
#include "ExchangeIdService.h"
#include "PocoSession.h"
#include "MySqlPixelService.h"
#include "BidRequestEnricher.h"
#include "EntityToModuleStateStats.h"
#include "OpenRtbBidRequest.h"
ActionRecorderLoadTester::ActionRecorderLoadTester(
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats) {
        sleeInMicroSecondsBetweenEachActionServerCall  =  std::make_shared<gicapods::AtomicLong>(10000000);
        session  = std::make_shared<PocoSession>(
                "ActionRecorderLoadTester",
                configService->getAsBooleanFromString("keepAliveMode"),
                configService->getAsInt("keepAliveTimeoutInMillis"),
                configService->getAsInt("timeoutInMillis"),
                configService->get("actionRecorderHostUrl"),
                entityToModuleStateStats);

        this->configService = configService;
}

void ActionRecorderLoadTester::setUp() {

}
std::string ActionRecorderLoadTester::sendRequestsToActionRecorder() {

}

void ActionRecorderLoadTester::startLoadTesting() {

        syncGoogleThread();

}

void ActionRecorderLoadTester::syncGoogleThread() {
        while(true) {
                try {
                        sendPixelActionToActionRecorder(10);
                        //we keep it low for now, because we have enough users 60000 in the cassandra and mysql
                        LOG_EVERY_N(ERROR, 10)<<"syncing random users for google";

                        sendRequestToGooglePixelMatching(10);
                        gicapods::Util::sleepMicroSecond(sleeInMicroSecondsBetweenEachActionServerCall->getValue());
                } catch(...) {
                        // gicapods::Util::showStackTrace();
                        LOG_EVERY_N(ERROR, 1000) << "error in syncing google thread";
                }

        }
}


std::shared_ptr<Pixel> ActionRecorderLoadTester::getRightPixelBasedOnDeviceId(std::string nomadiniDeviceId) {
        std::vector<std::shared_ptr<Pixel> > allPixels = mySqlPixelService->readAllEntities();

        long hashOfDevices = StringUtil::getHashedNumber(nomadiniDeviceId);
        //we want a pixel to have a consistent set of devices in it
        //this is done consistent with BidderLoadTester::setConsistentIabCatsPerDevicePercentageGroup
        //wich sets a consistent iab cat for devices...
        int groupOfDevice = std::abs(hashOfDevices) % allPixels.size() + 1;
        auto pixel = allPixels.at(groupOfDevice);
        return pixel;
}

void ActionRecorderLoadTester::sendPixelActionToActionRecorder(int numberOfPixelActions) {



        for (int i=0; i< numberOfPixelActions; i++) {
                try {
                        auto nomadiniDeviceId = exchangeIdService->getRandomeSyncedNomadiniDeviceId("google");
                        if (nomadiniDeviceId.empty()) {
                                continue;
                        }

                        auto pixel = getRightPixelBasedOnDeviceId(nomadiniDeviceId);

                        MLOG(3) << "sending nomadiniDeviceId : "<<nomadiniDeviceId <<" to pixel id "<< pixel->id;

                        LOG_EVERY_N(INFO, 1000) << google::COUNTER<<"th nomadiniDeviceId : "<<nomadiniDeviceId;
                        std::string actionRecorderUrl = "http://localhost:9989/pixelaction?pixelId=" + pixel->uniqueKey;
                        LOG_EVERY_N(INFO, 1000) << google::COUNTER<<"th actionRecorderUrl : "<<actionRecorderUrl;
                        Poco::Net::HTTPResponse response;
                        Poco::Net::HTTPRequest request (Poco::Net::HTTPRequest::HTTP_POST, actionRecorderUrl,
                                                        Poco::Net::HTTPMessage::HTTP_1_1);

                        //we set the cookie here and send request the action recorder
                        HttpUtil::sendGetRequestAndGetFullResponse(
                                HttpUtil::nomadiniDeviceId,
                                nomadiniDeviceId,
                                response,
                                actionRecorderUrl,
                                10000,
                                3);
                        entityToModuleStateStats->
                        addStateModuleForEntity("sent_action_request",
                                                "ActionRecorderLoadTester",
                                                EntityToModuleStateStats::all
                                                );
                        //TODO :figure out why this is the case!!
                        // std::string nomadiniDeviceIdCookie =
                        //         HttpUtil::getCookieFromResponse(response,
                        //                                         HttpUtil::nomadiniDeviceId);
                        // if(nomadiniDeviceIdCookie.empty()) {
                        //         LOG_EVERY_N(ERROR, 1) <<
                        //                 google::COUNTER << " nomadiniDeviceId was empty";
                        // } else {
                        //         LOG_EVERY_N(ERROR, 100) <<
                        //                 google::COUNTER << " nth pixel action called successfully";
                        // }

                } catch(...) {
                        LOG_EVERY_N(ERROR, 1000) << google::COUNTER << " exception in pixel action";
                        // gicapods::Util::showStackTrace();
                }
        }
}

//this method will sync 10000 random gooleIds using calling the googlePixelMatching in actionRecorder
void ActionRecorderLoadTester::sendRequestToGooglePixelMatching(int numberOfUsers) {


        for (int i=0; i< numberOfUsers; i++) {
                try{
                        std::string exchangeName = "google";
                        std::string firstPart = "googleId";
                        std::string googleId = StringUtil::random_string( firstPart, 12);
                        //TODO : read the properties from actionrecorder.properties
                        std::string actionRecorderUrl =
                                "http://localhost:9989/googlepixelmatching?google_gid=__NEW_GOOGLE_ID__&google_cver=1";
                        actionRecorderUrl =
                                StringUtil::replaceString(actionRecorderUrl,
                                                          "__NEW_GOOGLE_ID__",
                                                          googleId);
                        MLOG(3)<< "hitting actionRecorderUrl : "<< actionRecorderUrl;
                        Poco::Net::HTTPResponse response;
                        HttpUtil::sendGetRequestAndGetFullResponse(actionRecorderUrl,
                                                                   response,
                                                                   10000,
                                                                   3);
                        if(response.getStatus() != Poco::Net::HTTPResponse::HTTPStatus::HTTP_NO_CONTENT) {
                                LOG_EVERY_N(ERROR, 100)<< google::COUNTER<< "nth :  response status : "<< response.getStatus();

                                if (response.getStatus() != Poco::Net::HTTPResponse::HTTPStatus::HTTP_NO_CONTENT) {
                                        throwEx("response status is "+ response.getReasonForStatus(response.getStatus()));
                                }
                        }
                        entityToModuleStateStats->
                        addStateModuleForEntity("matched_device_id",
                                                "ActionRecorderLoadTester",
                                                EntityToModuleStateStats::all
                                                );
                        std::string nomadiniDeviceIdCookie =
                                HttpUtil::getCookieFromResponse(response, HttpUtil::nomadiniDeviceId);

                        //we create a device from the cookie to make sure
                        //the cookie is set correctly
                        auto device = std::make_shared<Device>(nomadiniDeviceIdCookie, "");

                        gicapods::Util::sleepMiliSecond(1);
                } catch(...) {
                        LOG_EVERY_N(ERROR, 100) << google::COUNTER << " exception in pixel matching";
                        // gicapods::Util::showStackTrace();
                }
        }

}
