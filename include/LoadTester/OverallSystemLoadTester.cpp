#include <cstdlib>
#include <exception>      // std::set_terminate

#include <chrono>
#include <thread>
#include "SignalHandler.h"
#include "OpenRtbBidRequest.h"
#include "BidderLoadTester.h"
#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequestParser.h"
#include "StackTraceUtil.h"
#include "RandomUtilDecimalGenerator.h"
#include <stack>
#include "DateTimeUtil.h"
#include "ActionRecorderLoadTester.h"
#include "BidderLoadTester.h"
#include "AdServerLoadTester.h"
#include "MySqlDriver.h"
#include "ExchangeIdService.h"
#include "OverallSystemLoadTester.h"
#include "EntityToModuleStateStats.h"
#include "RandomUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "EntityToModuleStateStats.h"
#include "RandomUtil.h"
#include "HttpUtil.h"
#include "Device.h"
#include "ConfigService.h"
#include "CollectionUtil.h"
bool OverallSystemLoadTester::loadTesterThreadCanSleep = false;
int OverallSystemLoadTester::sleepEveryNMillis = 10000;
int OverallSystemLoadTester::bidStrategy = 1;

OverallSystemLoadTester::OverallSystemLoadTester() {
        numberOfRequestInOneGo = 100;
        goodAdServerResponse =  std::make_shared<gicapods::AtomicLong>();
        syncedGoogleId = std::make_shared<gicapods::AtomicLong>();
        sleeInMicroSecondsBetweenEachBidderCall = std::make_shared<gicapods::AtomicLong>(1);
        priceRandomizer = std::make_shared<RandomUtilDecimalGenerator> (1, 2);

}

void OverallSystemLoadTester::run() {
        bidderLoadTester->sendRequestsToBidder();
}
