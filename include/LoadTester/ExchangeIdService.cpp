
#include "ExchangeIdService.h"

#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "Site.h"
#include "EntityToModuleStateStats.h"
#include "GicapodsIdToExchangeIdsMapCassandraService.h"
#include "MySqlCreativeService.h"
#include "HttpUtilService.h"

#include "Segment.h"
#include "Inventory.h"
#include "ModelRequestTestHelper.h"
#include "ModelRequest.h"
#include "Advertiser.h"
#include "RandomUtil.h"
#include "SiteTestHelper.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "TempUtil.h"
#include "AtomicLong.h"
#include "GicapodsIdToExchangeIdsMap.h"

ExchangeIdService::ExchangeIdService() {
        numberOfSyncedIdsToRead = 1000;
        exchangeToSyncedIdsReadFromMysql =
                std::make_shared<tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > > >();


}
std::string ExchangeIdService::getRandomeSyncedNomadiniDeviceId(std::string exchangeName) {
        NULL_CHECK(exchangeToSyncedIdsReadFromMysql);
        tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor;

        if(!exchangeToSyncedIdsReadFromMysql->find(accessor, exchangeName)) {
                exchangeToSyncedIdsReadFromMysql->insert(accessor, exchangeName);
                accessor->second = std::make_shared<std::set<std::string> >();
        }

        if (exchangeToSyncedIdsReadFromMysql->empty()) {
                LOG(ERROR) << "returning without any id";
                return "";
        }

        //we get the random number from 0 to 99
        //user 0 to 99 used for action taker devices
        if (exchangeToSyncedIdsReadFromMysql->empty()) {
                LOG(ERROR) << "returning without any id";
                return "";
        }
        //we get the random number from 0 to 1000
        //user 0 to 10000 are used for bidding
        int size = accessor->second->size() - 1;
        LOG_EVERY_N(ERROR,1000 ) << google::COUNTER<< "size of ids available: "<< size;
        if (size <= 0) {
                return "";
        }
        int randomNumber = RandomUtil::sudoRandomNumber(size);
        if (randomNumber > 99) {
                randomNumber = RandomUtil::sudoRandomNumber(100);
        }
        if (randomNumber >= accessor->second->size()) {
                return "";
        }

        std::set<std::string>::iterator it = accessor->second->begin();

        std::advance(it, randomNumber);

        std::string randomeSyncedNomadiniDeviceId = *it;
        auto exchangeIdAndNomadiniDeviceIdVector = StringUtil::tokenizeString(randomeSyncedNomadiniDeviceId, "___");

        if (exchangeIdAndNomadiniDeviceIdVector.size() != 2) {
                LOG(ERROR) << "bad exchangeIdAndNomadiniDeviceIdVector pair : "<<
                        randomeSyncedNomadiniDeviceId;
                exchangeToSyncedIdsReadFromMysql->erase(accessor);
        }
        assertAndThrow(exchangeIdAndNomadiniDeviceIdVector.size() == 2);
        randomeSyncedNomadiniDeviceId = exchangeIdAndNomadiniDeviceIdVector.at(1);
        MLOG(3)<<"randomeSyncedNomadiniDeviceId : "<< randomeSyncedNomadiniDeviceId
               << " , randomNumber : " << randomNumber;
        return randomeSyncedNomadiniDeviceId;
}
std::string ExchangeIdService::getRandomeSyncedBuyerId(std::string exchangeName) {
        NULL_CHECK(exchangeToSyncedIdsReadFromMysql);
        tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor;

        if(!exchangeToSyncedIdsReadFromMysql->find(accessor, exchangeName)) {
                exchangeToSyncedIdsReadFromMysql->insert(accessor, exchangeName);
                accessor->second = std::make_shared<std::set<std::string> >();
        }
        if (exchangeToSyncedIdsReadFromMysql->empty()) {
                LOG(ERROR) << "returning without any id";
                return "";
        }
        std::set<std::string>::iterator it = accessor->second->begin();
        //we get the random number from 0 to 1000
        //user 0 to 10000 are used for bidding
        int size = accessor->second->size() - 1;
        if (size <= 0) {
                LOG_EVERY_N(ERROR,1000) << google::COUNTER<< " size of ids available:  "<< size;
                return "";
        }
        LOG_EVERY_N(ERROR,100000) << google::COUNTER<< "th time :  size of ids available:  "<< size;

        int randomNumber = RandomUtil::sudoRandomNumber(size);
        if (randomNumber > size) {
                randomNumber = RandomUtil::sudoRandomNumber(size);
        }
        if (randomNumber >= accessor->second->size()) {
                LOG_EVERY_N(ERROR, 1000)<< google::COUNTER << "th : returning no userId for bidreuqest";
                return "";

        }
        std::advance(it, randomNumber);
        std::string randomeBuyerId = *it;
        auto exchangeIdAndNomadiniDeviceIdVector = StringUtil::tokenizeString(randomeBuyerId, "___");
        if (exchangeIdAndNomadiniDeviceIdVector.size() != 2) {
                LOG(ERROR) << "bad randomeBuyerId pair : "<<
                        randomeBuyerId;
                exchangeToSyncedIdsReadFromMysql->erase(accessor);
                return "";
        }
        assertAndThrow(exchangeIdAndNomadiniDeviceIdVector.size() == 2);
        randomeBuyerId = exchangeIdAndNomadiniDeviceIdVector.at(1);
        LOG_EVERY_N(INFO, 10000)<<"randomeBuyerId : " << randomeBuyerId << " , randomNumber : " << randomNumber;
        return randomeBuyerId;
}
void ExchangeIdService::setup() {
        auto allSyncedGoogleIds = std::make_shared<std::set<std::string> >();
        auto allSyncedRubiconIds = std::make_shared<std::set<std::string> >();

        tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor;
        exchangeToSyncedIdsReadFromMysql->insert(accessor, "google");
        accessor->second = allSyncedGoogleIds;

        tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor1;
        exchangeToSyncedIdsReadFromMysql->insert(accessor1, "rubicon");
        accessor1->second = allSyncedRubiconIds;
}
void ExchangeIdService::insertPairOfIdsIntoOutgoingMap(
        std::string exchangeName, std::string nomadiniDeviceIdCookie, std::string googleId ) {
        tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor;

        if (!exchangeToSyncedIdsReadFromMysql->find(accessor, exchangeName)) {
                exchangeToSyncedIdsReadFromMysql->insert(accessor, exchangeName);
                accessor->second = std::make_shared<std::set<std::string> >();
        }

        accessor->second->insert(googleId + "___" + nomadiniDeviceIdCookie);
}

void ExchangeIdService::refreshSyncedIdsFromMySql(bool continuous) {
        do {
                //wait 3 seconds for cassandra to come up
                gicapods::Util::sleepViaBoost(_L_, 3);

                auto allSyncedIdMaps =
                        std::make_shared<std::vector<std::shared_ptr<GicapodsIdToExchangeIdsMap> > > ();
                // we refresh the sync
                //we retry until we have data
                do {
                        try {
                                allSyncedIdMaps =
                                        gicapodsIdToExchangeIdsMapCassandraService->readWithPaging(
                                                1000, numberOfSyncedIdsToRead);
                                break;
                        } catch (...) {
                                gicapods::Util::showStackTrace();
                                gicapods::Util::sleepViaBoost(_L_, 1);
                        }
                } while(allSyncedIdMaps->empty());

                exchangeToSyncedIdsReadFromMysql->clear();
                MLOG(3)<<"refreshing exchangeSyncedIds ";

                LOG(ERROR)<<"loaded allSyncedIdMaps from cassandra "<<  allSyncedIdMaps->size();
                for (auto gicapodsIdToExchangeIdsMap : *allSyncedIdMaps) {
                        for (auto exchangeNameToIdPair : gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId) {
                                auto exchangeName = exchangeNameToIdPair.first;
                                auto exchangeId = exchangeNameToIdPair.second;
                                auto nomadiniDeviceId = gicapodsIdToExchangeIdsMap->nomadiniDeviceId;
                                insertPairOfIdsIntoOutgoingMap(exchangeName, nomadiniDeviceId, exchangeId);
                        }
                }

                for (auto&& pair : *exchangeToSyncedIdsReadFromMysql) {
                        auto exchangeName = pair.first;
                        LOG(ERROR)<<"loaded  "<<  pair.second->size()<< " synced ids from mysql for exchange "<< exchangeName;
                }

                if (continuous) {
                        gicapods::Util::sleepViaBoost(_L_, 100);
                }
        } while(continuous);

}
ExchangeIdService::~ExchangeIdService() {
}
