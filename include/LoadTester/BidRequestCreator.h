#ifndef BidRequestCreator_H
#define BidRequestCreator_H

#include "Poco/URI.h"
#include "OpenRtbBidRequest.h"
#include "RandomUtilDecimalGenerator.h"
namespace gicapods { class ConfigService; }
namespace OpenRtb2_3_0 { class OpenRtbBidRequest; }
#include "BidRequestEnricher.h"
#include <memory>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "AtomicLong.h"

class RandomUtil;
class BidRequestCreator {

public:
std::shared_ptr<RandomUtilDecimalGenerator> usLatRandomizer;

std::shared_ptr<RandomUtilDecimalGenerator> usLonRandomizer;

BidRequestCreator();
virtual ~BidRequestCreator();
std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> createOpenRtbBidRequest();
};

#endif
