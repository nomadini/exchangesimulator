#include "GUtil.h"
#include "FileUtil.h"
#include "OpenRtbBidRequest.h"
#include "CassandraDriverInterface.h"
#include "AdServerLoadTester.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include "ConverterUtil.h"
#include "NetworkUtil.h"
#include "BidRequestEnricher.h"
#include <Util/StatisticsUtil.h>
#include "MySqlBWListService.h"
#include "RandomUtilDecimalGenerator.h"
#include "ConfigService.h"
#include "RandomUtil.h"
#include "StringUtil.h"
#include "Pixel.h"
#include <thread>
#include "Device.h"
#include "ExchangeIdService.h"
#include "MySqlPixelService.h"
#include "BidRequestEnricher.h"
#include "OpenRtbBidRequest.h"
#include "EventLog.h"
#include "RandomUtilDecimalGenerator.h"
#include "JsonArrayUtil.h"
#include "NomadiniSessionInterface.h"
#include "PocoSession.h"
#include "AeroCacheService.h"
AdServerLoadTester::AdServerLoadTester(gicapods::ConfigService* configService,
                                       EntityToModuleStateStats* entityToModuleStateStats) {
        priceRandomizer = std::make_shared<RandomUtilDecimalGenerator> (1, 2);
        this->configService = configService;
        session  = std::make_shared<PocoSession>(
                "AdServerLoadTester",
                configService->getAsBooleanFromString("keepAliveMode"),
                configService->getAsInt("keepAliveTimeoutInMillis"),
                configService->getAsInt("timeoutInMillis"),
                configService->get("adServerHostUrl"),
                entityToModuleStateStats);

}

void AdServerLoadTester::setUp() {

}

void AdServerLoadTester::callConversionPixel(std::shared_ptr<EventLog> eventLog) {
//conv-pix-action
//ActionRecorderLoadTester->sendRequestToConversionHandler(
}

void AdServerLoadTester::startLoadTestingAdServer() {

        LOG(ERROR) << "adserv load testing is disabled...use another adserver for loadtesting";
        return;

        auto eventLogInJson =
                FileUtil::readFileInString("/home/vagrant/workspace/ExchangeSimulator/resources/sampleEventLog.txt");
        while(true) {
                try {
                        //don't run this...we need real testing now, not this load testing
                        return;
                        auto eventLogs = prepareEvents(eventLogInJson);
                        for (auto eventLog : *eventLogs) {
                                startLoadTestingImpressionHanlder(eventLog);
                                startLoadTestingImpTracker(eventLog);
                                startLoadTestingClickTracker(eventLog);
                                callConversionPixel(eventLog);
                        }
                } catch(std::exception const &e) {
                        gicapods::Util::showStackTrace(&e);
                        LOG_EVERY_N(ERROR, 1) << "error sending direct request to adserver";
                }
                // gicapods::Util::sleepViaBoost(_L_, 1);
                //we want to test how many impressions can mysql handle
                gicapods::Util::sleepMicroSecond(10000000);
        }

}

std::shared_ptr<std::vector<std::shared_ptr<EventLog> > > AdServerLoadTester::prepareEvents(std::string eventLogInJson) {
        std::shared_ptr<std::vector<std::shared_ptr<EventLog> > > allEventLogs =
                JsonArrayUtil::convertStringArrayToArrayOfObject<EventLog>(eventLogInJson);

        for (auto eventLog : *allEventLogs) {
                int randomNumber = RandomUtil::sudoRandomNumber(100000);
                eventLog->eventId += StringUtil::toStr(randomNumber) + "-test-exch-sm";

                exchangeBeanFactory->realTimeEventLogCacheService->putDataInCache(eventLog);
                int cacheResultStatus = 0;
                auto event = exchangeBeanFactory->realTimeEventLogCacheService->readDataOptional(eventLog, cacheResultStatus);
                NULL_CHECK(event);
        }

        return allEventLogs;
}

void AdServerLoadTester::startLoadTestingImpressionHanlder(std::shared_ptr<EventLog> eventLog) {
        try {
                std::string impWinUrl =
                        "http://67.205.132.35:9981/win?won="+
                        _toStr(eventLog->winBidPrice + priceRandomizer->randomDecimalV2())
                        +"&transactionId=" + eventLog->eventId;

                LOG(INFO) << "impWinUrl : "<<impWinUrl;
                Poco::Net::HTTPResponse response;
                Poco::Net::HTTPRequest request (Poco::Net::HTTPRequest::HTTP_POST, impWinUrl,
                                                Poco::Net::HTTPMessage::HTTP_1_1);

                //we set the cookie here and send request the action recorder
                std::string responseStr =
                        HttpUtil::sendGetRequestAndGetFullResponse(
                                "",
                                "",
                                response,
                                impWinUrl,
                                10000,
                                3);
                LOG_EVERY_N(ERROR, 1) << " adserver creative response: "<< responseStr;
        } catch(std::exception const &e) {
                gicapods::Util::showStackTrace(&e);
        }
}

void AdServerLoadTester::startLoadTestingImpTracker(std::shared_ptr<EventLog> eventLog) {
        try {
                std::string impTrackerUrl =
                        "http://67.205.132.35:9981/queue0-imptrk?transactionId="+
                        eventLog->eventId
                        +"&CACHE_BUSTER=2956602";

                LOG(INFO) << "impTrackerUrl : "<<impTrackerUrl;
                Poco::Net::HTTPResponse response;
                Poco::Net::HTTPRequest request (Poco::Net::HTTPRequest::HTTP_POST, impTrackerUrl,
                                                Poco::Net::HTTPMessage::HTTP_1_1);

                std::string cookieKey = "";
                std::string cookieValue = "";
                //we set the cookie here and send request the action recorder
                std::string responseStr =
                        HttpUtil::sendGetRequestAndGetFullResponse(
                                cookieKey,
                                cookieValue,
                                response,
                                impTrackerUrl,
                                10000,
                                3);
                LOG_EVERY_N(ERROR, 1) << " adserver impTrackerUrl response: "<< responseStr;
        } catch(std::exception const &e) {
                gicapods::Util::showStackTrace(&e);
        }
}

void AdServerLoadTester::startLoadTestingClickTracker(std::shared_ptr<EventLog> eventLog) {
        try {
                // string clickTrackerDestinationUrl = "https://servedby.flashtalking.com/click/3/57363;1633661;0;209;0/?ft_width=160&ft_height=600&url=8437998";
                std::string clickTrackerDestinationUrl = "http://67.205.132.35:9981/queue0-dest-clk?transactionId="+ eventLog->eventId;

                std::string clickUrl =
                        "http://67.205.132.35:9981/queue0-clk?transactionId="+
                        eventLog->eventId
                        +"&CACHE_BUSTER=2956602&ctrack=" + clickTrackerDestinationUrl;

                LOG(INFO) << "clickUrl : "<<clickUrl;
                Poco::Net::HTTPResponse response;
                Poco::Net::HTTPRequest request (Poco::Net::HTTPRequest::HTTP_POST, clickUrl,
                                                Poco::Net::HTTPMessage::HTTP_1_1);

                //we set the cookie here and send request the action recorder
                std::string responseStr =
                        HttpUtil::sendGetRequestAndGetFullResponse(
                                "",
                                "",
                                response,
                                clickUrl,
                                10000,
                                3);
                LOG_EVERY_N(ERROR, 1) << " adserver clickUrl response: "<< responseStr;
        } catch(std::exception const &e) {
                gicapods::Util::showStackTrace(&e);
        }
}


std::string AdServerLoadTester::sendWinNotificationsToAdserv(std::string notificationUrl) {
        auto response = session->sendRequest(
                *PocoHttpRequest::createSimplePostRequest(
                        notificationUrl,
                        ""));
        return response->body;
}
