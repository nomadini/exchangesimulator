#include <cstdlib>
#include <exception>      // std::set_terminate

#include <chrono>
#include <thread>
#include "SignalHandler.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequestParser.h"
#include "StackTraceUtil.h"
#include "Poco/Runnable.h"
#include <stack>
#include "Poco/Util/PropertyFileConfiguration.h"

class MySqlDriver;
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include <set>
#include "BidRequestEnricher.h"
namespace gicapods { class ConfigService; }
#include "Poco/Net/HTTPResponse.h"
#include "AtomicLong.h"

class EntityToModuleStateStats;
class BidderLoadTester;
class ExchangeIdService;
class AdServerLoadTester;
class ActionRecorderLoadTester;
class RandomUtilDecimalGenerator;

class OverallSystemLoadTester : public Poco::Runnable {
public:
Poco::AutoPtr<Poco::Util::PropertyFileConfiguration> pConf;
std::shared_ptr<RandomUtilDecimalGenerator> priceRandomizer;
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
ExchangeIdService* exchangeIdService;
int numberOfRequestInOneGo;
OverallSystemLoadTester();

BidderLoadTester* bidderLoadTester;
AdServerLoadTester* adServerLoadTester;
ActionRecorderLoadTester* actionRecorderLoadTester;

std::shared_ptr<gicapods::AtomicLong> sleeInMicroSecondsBetweenEachBidderCall;
std::shared_ptr<gicapods::AtomicLong> goodAdServerResponse;
std::shared_ptr<gicapods::AtomicLong> syncedGoogleId;

static int sleepEveryNMillis;

static bool loadTesterThreadCanSleep;

static int bidStrategy;

virtual void run();

};
