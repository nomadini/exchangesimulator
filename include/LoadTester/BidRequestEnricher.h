//
// Created by Mahmoud Taabodi on 2/12/16.
//

#ifndef GICAPODS_BIDREQUESTENRICHER_H
#define GICAPODS_BIDREQUESTENRICHER_H


#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "Poco/Net/HTTPResponse.h"

class MySqlDriver;
class TargetGroupBWListCacheService;
class ExchangeIdService;
class MySqlGlobalWhiteListService;
namespace OpenRtb2_3_0 {
class OpenRtbBidRequest;
class Site;
class Banner;
}

class SiteTestHelper;
class GlobalWhiteListEntry;

/**
   this class is used by BidderLoadTester to customize a bid request
 */
class BidRequestEnricher;



class BidRequestEnricher {

public:
MySqlGlobalWhiteListService* mySqlGlobalWhiteListService;
std::vector<std::shared_ptr<GlobalWhiteListEntry> > allWhiteListedEntries;

TargetGroupBWListCacheService* targetGroupBWListCacheService;
BidRequestEnricher(MySqlDriver* mySqlDriver,
                   MySqlGlobalWhiteListService* mySqlGlobalWhiteListService,
                   TargetGroupBWListCacheService* targetGroupBWListCacheService);

std::vector<std::shared_ptr<OpenRtb2_3_0::Banner> > banners;
std::shared_ptr<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > > getKnownSites();
void setNoBuyerId(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);
std::shared_ptr<std::vector<std::shared_ptr<OpenRtb2_3_0::Site> > > getUnKnownSites();
ExchangeIdService* exchangeIdService;

int getRandomDeviceType();

std::string getAWhiteListedGlobalDomain();
std::string getARandomBadDomain();

std::string getRandomCreativeAPI();
std::string getRandomCreativeAdType();

std::shared_ptr<OpenRtb2_3_0::Banner> getRandomBanner();


void setRandomCreativeSize(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);
void setBlockedAdvertiser(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);

void setKnownBuyerId(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest,
                     std::string exchangeName);

void setWhiteListedSite(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);

void setBlackListedSite(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);
};


#endif //GICAPODS_BIDREQUESTENRICHER_H
