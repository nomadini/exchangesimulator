#ifndef ActionRecorderLoadTester_H
#define ActionRecorderLoadTester_H

#include "Poco/URI.h"
namespace OpenRtb2_3_0 { class OpenRtbBidRequest; }
namespace gicapods { class ConfigService; }

#include <memory>
#include <string>
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Encoder.h"
#include "Pixel.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/URI.h"
#include "BidderLoadTester.h"
#include "AtomicLong.h"
#include "PocoSession.h"
#include <tbb/concurrent_hash_map.h>

class NomadiniSessionInterface;
class BidRequestEnricher;
class ExchangeIdService;
class MySqlPixelService;
class EntityToModuleStateStats;

class ActionRecorderLoadTester : public std::enable_shared_from_this<ActionRecorderLoadTester> {

public:
std::shared_ptr<gicapods::AtomicLong> sleeInMicroSecondsBetweenEachActionServerCall;
Poco::URI loadTesterUri;
std::shared_ptr<PocoSession> session;
ExchangeIdService* exchangeIdService;
gicapods::ConfigService* configService;
MySqlPixelService* mySqlPixelService;
EntityToModuleStateStats* entityToModuleStateStats;
ActionRecorderLoadTester(gicapods::ConfigService* configService,
                         EntityToModuleStateStats* entityToModuleStateStats);
void setUp();

BidderLoadTester* bidderLoadTester;

std::string sendRequestsToActionRecorder();
void syncGoogleThread();

void sendRequestToGooglePixelMatching(int numberOfUsers);

void startLoadTesting();

void sendPixelActionToActionRecorder(int numberOfPixelActions);
std::shared_ptr<Pixel> getRightPixelBasedOnDeviceId(std::string nomadiniDeviceId);
};





#endif
