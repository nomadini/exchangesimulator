
#include "ExchangeSimulator.h"

#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "Site.h"
#include "ServiceFactory.h"
#include "ConverterUtil.h"
#include "PocoHttpServer.h"
#include "OverallSystemLoadTester.h"
#include "MySqlBWEntryService.h"
#include "MySqlPixelService.h"
#include "MySqlBWListService.h"
#include "AdServerLoadTester.h"

#include "TargetGroupGeoLocation.h"
#include "TargetGroupGeoSegmentListMap.h"
#include "TargetGroupSegmentMap.h"
#include "EntityToModuleStateStats.h"
#include "EntityToModuleStateStats.h"

#include "Segment.h"
#include "Inventory.h"
#include "ModelRequestTestHelper.h"
#include "ModelRequest.h"
#include "Advertiser.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"
#include "GeoSegmentList.h"
#include "BWList.h"
#include "Creative.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroup.h"
#include "RandomUtil.h"
#include "SiteTestHelper.h"
#include "MaxMindService.h"
#include "BidRequestEnricher.h"
#include "AdServerCaller.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "TempUtil.h"
#include "AtomicLong.h"
#include "ActionRecorderLoadTester.h"
#include "AsyncThreadPoolService.h"

#include "GicapodsIdToExchangeIdsMapCassandraService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "MySqlTargetGroupInventoryMapService.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoLocationService.h"

#include "MySqlCreativeService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCampaignService.h"
#include "MySqlAdvertiserService.h"
#include "CampaignCacheService.h"
#include "CreativeCacheService.h"
#include "HttpUtilService.h"

#include "EntityToModuleStateStats.h"

#include "MySqlCreativeService.h"
#include "HttpUtilService.h"
#include "MySqlGlobalWhiteListService.h"
#include "ConcurrentQueueFolly.h"
#include "BidRequestEnricher.h"
#include "ExchangeIdService.h"
#include "ExchangeBeanFactory.h"
#include "BidderLoadTester.h"
#include "UserAgentTestHelper.h"
#include "JsonArrayUtil.h"
#include "EntityToModuleStateStatsPersistenceService.h"
ExchangeSimulator::ExchangeSimulator(
        std::unique_ptr<ExchangeBeanFactory> exchangeBeanFactoryArg,
        std::unique_ptr<ServiceFactory> serviceFactoryArg,
        std::unique_ptr<ExchangeIdService> exchangeIdServiceArg) {
        this->exchangeBeanFactory = std::move(exchangeBeanFactoryArg);
        this->serviceFactory = std::move(serviceFactoryArg);
        this->exchangeIdService = std::move(exchangeIdServiceArg);

        entityToModuleStateStats = exchangeBeanFactory->entityToModuleStateStats.get();
        dataReloadService = serviceFactory->dataReloadService.get();

        UserAgentTestHelper::getRandomUserAgentFromFiniteSet();//to initialize the list


        actionRecorderLoadTester =
                std::make_shared<ActionRecorderLoadTester>(
                        exchangeBeanFactory->configService.get(),
                        exchangeBeanFactory->entityToModuleStateStats.get()
                        );

        actionRecorderLoadTester->configService = exchangeBeanFactory->configService.get();
        actionRecorderLoadTester->entityToModuleStateStats = exchangeBeanFactory->entityToModuleStateStats.get();
        actionRecorderLoadTester->exchangeIdService = exchangeIdService.get();
        actionRecorderLoadTester->mySqlPixelService = exchangeBeanFactory->mySqlPixelService.get();
        actionRecorderLoadTester->setUp();


        bidderResponses = std::make_shared<ConcurrentQueueFolly<std::string> >();

        adServerLoadTester = std::make_shared<AdServerLoadTester>(
                exchangeBeanFactory->configService.get(),
                exchangeBeanFactory->entityToModuleStateStats.get());
        adServerLoadTester->exchangeBeanFactory = exchangeBeanFactory.get();
        adServerLoadTester->setUp();


        auto bidderHostUrl = exchangeBeanFactory->configService->get ("bidderHostUrl");

        bidRequestEnricher = std::make_unique<BidRequestEnricher>(
                exchangeBeanFactory->mySqlDriverMango.get(),
                exchangeBeanFactory->mySqlGlobalWhiteListService.get(),
                exchangeBeanFactory->targetGroupBWListCacheService.get()
                );
        bidRequestEnricher->exchangeIdService =
                exchangeIdService.get();

        this->bidderLoadTester = std::make_unique<BidderLoadTester>(
                "exchangesimulator.properties",
                bidRequestEnricher.get(),
                exchangeBeanFactory->configService.get(),
                exchangeBeanFactory->entityToModuleStateStats.get(),
                "google",
                10000,
                bidderHostUrl);
        this->bidderLoadTester->entityToModuleStateStats = entityToModuleStateStats;


        configService = exchangeBeanFactory->configService.get();
        exchangeSimulatorRequestHandlerFactory =
                std::make_shared<ExchangeSimulatorRequestHandlerFactory>(entityToModuleStateStats);
        exchangeSimulatorRequestHandlerFactory->commonRequestHandlerFactory = serviceFactory->commonRequestHandlerFactory.get();

        entityToModuleStateStatsPersistenceService = exchangeBeanFactory->entityToModuleStateStatsPersistenceService.get();

        exchangeIdService->gicapodsIdToExchangeIdsMapCassandraService =
                exchangeBeanFactory->
                gicapodsIdToExchangeIdsMapCassandraService.get();
        exchangeIdService->setup();
        exchangeIdService->refreshSyncedIdsFromMySql(false); //reading the synced ids for the first time

        bidderLoadTester->prepareRequests();//preparing batch of requests after ids are read from cassandra
}

ExchangeSimulator::~ExchangeSimulator() {

}

std::vector<std::shared_ptr<Creative> > ExchangeSimulator::insertCreatives(MySqlCreativeService* mySqlCreativeService,
                                                                           std::string uniqueName,
                                                                           int advertiserId) {
        std::vector<std::shared_ptr<Creative> > allCreativesInserted;
        for(int i=0; i< 5; i++) {
                auto creative = CreativeTestHelper::createSampleCreative();
                assertAndThrow(creative->getId () > 0);

                creative->setAdType(bidRequestEnricher->getRandomCreativeAdType());
                creative->setAdvertiserId(advertiserId);
                // creative->setApis(std::make_shared<std::vector<std::string> >(CollectionUtil::convertToList(
                //                                                                       bidRequestEnricher->getRandomCreativeAPI())));

                // creative->setSize(bidRequestEnricher->getRandomCreativeSize());
                mySqlCreativeService->insert(creative);
                allCreativesInserted.push_back(creative);
        }

        return allCreativesInserted;
}


int ExchangeSimulator::insertBwList(std::string uniqueName, std::string listType, int clientId) {
        std::shared_ptr<BWList> bwList = std::make_shared<BWList>();
        std::string uniqueNameForList = StringUtil::toStr(RandomUtil::sudoRandomNumber(10000000));

        std::string listName = StringUtil::toStr(listType);
        listName.append(uniqueNameForList);
        listName.append(uniqueName);

        bwList->setName(listName);
        bwList->setListType(listType);//whitelist or blacklist
        bwList->setClientId(clientId);
        exchangeBeanFactory->mySqlBWListService->insert(bwList);
        assertAndThrow(bwList->getId() > 0);
        for (int i=0; i < 5; i++) {
                std::shared_ptr<BWEntry> entry = std::make_shared<BWEntry>();

                std::string domainName = "abc";
                domainName.append(StringUtil::toStr(RandomUtil::sudoRandomNumber(10000000)));
                domainName.append(StringUtil::toStr(i));
                domainName.append(StringUtil::toStr(".com"));
                entry->setDomainName(domainName);
                entry->setBwListId (bwList->getId ());
                exchangeBeanFactory->mySqlBWEntryService->insert(entry);
                assertAndThrow(entry->getId () > 0);
        }
        return bwList->getId();
}


void ExchangeSimulator::insertRandomGlobalWhiteListDomains() {
        std::vector<std::string> goodDomains = {"abc.com", "cnn.com", "foxnew.com"
                                                , "balatarin.com", "facebook.com", "twitter.com"
                                                "pokemon.com", "immigration.com", "bostonGlobe.com", "vanityFair.com"};
        for(int i=0; i< goodDomains.size(); i++) {
                exchangeBeanFactory->mySqlGlobalWhiteListService->insert(goodDomains.at(i));
        }
}


std::shared_ptr<OverallSystemLoadTester> ExchangeSimulator::
createOverallLoadTester( )
{

        auto overallSystemLoadTester = std::make_shared<OverallSystemLoadTester>();
        overallSystemLoadTester->configService =  exchangeBeanFactory->configService.get();
        overallSystemLoadTester->numberOfRequestInOneGo =  exchangeBeanFactory->configService->getAsInt("numberOfRequestInOneGo");

        overallSystemLoadTester->entityToModuleStateStats =  exchangeBeanFactory->entityToModuleStateStats.get();
        int sleeInMicroSecondsBetweenEachBidderCall = ConverterUtil::convertTo<int>(exchangeBeanFactory->configService->get("sleeInMicroSecondsBetweenEachBidderCall"));
        overallSystemLoadTester->sleeInMicroSecondsBetweenEachBidderCall->setValue(sleeInMicroSecondsBetweenEachBidderCall);
        overallSystemLoadTester->entityToModuleStateStats = exchangeBeanFactory->entityToModuleStateStats.get();
        overallSystemLoadTester->bidderLoadTester = bidderLoadTester.get();
        overallSystemLoadTester->actionRecorderLoadTester = actionRecorderLoadTester.get();
        overallSystemLoadTester->adServerLoadTester = adServerLoadTester.get();
        overallSystemLoadTester->bidderLoadTester->bidderResponses = bidderResponses;
        return overallSystemLoadTester;
}

std::shared_ptr<AdServerCaller> ExchangeSimulator::createAdServerCaller() {

        auto adServerCaller = std::make_shared<AdServerCaller>();
        adServerCaller->configService =  exchangeBeanFactory->configService.get();
        adServerCaller->entityToModuleStateStats =  exchangeBeanFactory->entityToModuleStateStats.get();
        int sleeInMicroSecondsBetweenEachBidderCall = ConverterUtil::convertTo<int>(exchangeBeanFactory->configService->get("sleeInMicroSecondsBetweenEachBidderCall"));
        adServerCaller->sleeInMicroSecondsBetweenEachBidderCall->setValue(sleeInMicroSecondsBetweenEachBidderCall);
        adServerCaller->entityToModuleStateStats = exchangeBeanFactory->entityToModuleStateStats.get();
        adServerCaller->actionRecorderLoadTester = actionRecorderLoadTester.get();
        adServerCaller->bidderResponses = bidderResponses;
        adServerCaller->adServerLoadTester = adServerLoadTester.get();

        return adServerCaller;
}

void ExchangeSimulator::startOverallSystemLoadTesting() {

        int numberOfTasksToSubmitToBidderHammerPool =
                exchangeBeanFactory->configService->getAsInt ("numberOfTasksToSubmitToBidderHammerPool");
        LOG(INFO)<<"starting "<<numberOfTasksToSubmitToBidderHammerPool<< " load test task";
        int minCapacity = exchangeBeanFactory->configService->getAsInt("appThreadPoolMinCapacity");
        int maxCapacity = exchangeBeanFactory->configService->getAsInt("appThreadPoolMaxCapacity");
        int idleTime = exchangeBeanFactory->configService->getAsInt("appThreadPoolIdleTimeInSeconds");
        int stackSize = exchangeBeanFactory->configService->getAsInt("appThreadPoolStackSize");

        auto threadPool = new Poco::ThreadPool(
                minCapacity,
                maxCapacity,
                idleTime,
                stackSize
                );


        std::shared_ptr<OverallSystemLoadTester> overallSystemLoadTester = createOverallLoadTester();




        while(true) {
                try {
                        for (int i = 0; i < numberOfTasksToSubmitToBidderHammerPool; i++) {
                                threadPool->start(*overallSystemLoadTester);
                        }


                        threadPool->joinAll();
                } catch(...) {
                        LOG_EVERY_N(ERROR, 1)<<"error happened";
                }
                gicapods::Util::sleepViaBoost(_L_,
                                              exchangeBeanFactory->configService->getAsInt ("secondsToSleepBetweenEachBidderLoadtestThread"));
        }
}

void ExchangeSimulator::persistStats() {
        while(true) {
                try {
                        entityToModuleStateStats->addStateModuleForEntity ("persistStats",
                                                                           "ExchangeSimulator",
                                                                           "ALL");

                        this->entityToModuleStateStatsPersistenceService->persistStats();

                        bidderLoadTester->prepareRequests();

                        entityToModuleStateStats->clearAllMetrics();

                        dataReloadService->reloadDataViaHttp();

                } catch (std::exception const &e) {
                        //TODO : go red and show in dashboard that this is red
                        gicapods::Util::showStackTrace();
                        LOG(ERROR) << "error happening when persistStats  " << boost::diagnostic_information (e);
                }
                gicapods::Util::sleepViaBoost (_L_, 30);
        }
}

int ExchangeSimulator::main(const std::vector<std::string>& args) {



        auto srv = PocoHttpServer::createHttpServer(configService,
                                                    exchangeSimulatorRequestHandlerFactory.get());

        // start the HTTPServer
        LOG(INFO)<<"ExchangeSimulator server about to start ";
        srv->start();
        // wait for CTRL-C or kill
        waitForTerminationRequest();
        // Stop the HTTPServer
        srv->stop();

        return Application::EXIT_OK;
}

void ExchangeSimulator::startAdServerCallerThread() {
        std::shared_ptr<AdServerCaller> adServerCaller = createAdServerCaller();
        adServerCaller->run();
}

int main(int argc, char** argv) {


        std::string propertyFileName = "exchangesimulator.properties";
        std::string appName = "ExchangeSimulator";

        TempUtil::configureLogging (appName, argv);
        SiteTestHelper::populateDomainList();

        auto exchangeBeanFactory = std::make_unique<ExchangeBeanFactory>("SNAPSHOT");
        exchangeBeanFactory->propertyFileName = propertyFileName;
        exchangeBeanFactory->commonPropertyFileName = "common.properties";
        exchangeBeanFactory->appName = appName;

        exchangeBeanFactory->initializeModules();
        auto serviceFactory = std::make_unique<ServiceFactory>(
                exchangeBeanFactory->entityToModuleStateStats.get(),
                exchangeBeanFactory->configService.get(),
                exchangeBeanFactory->bootableConfigService.get(),
                exchangeBeanFactory->allCacheServices,
                exchangeBeanFactory->allEntityProviderService
                );
        serviceFactory->initializeModules();

        auto loadTestActionRecorderProp =
                exchangeBeanFactory->configService->get ("loadTestActionRecorder");

        auto loadTestAdServerProp =
                exchangeBeanFactory->configService->get ("enableLoadTestingAdServer");


        auto exchangeIdService = std::make_unique<ExchangeIdService>();
        exchangeIdService->numberOfSyncedIdsToRead =
                exchangeBeanFactory->configService->getAsInt ("numberOfSyncedIdsToRead");

        std::unique_ptr<ExchangeSimulator> exchangeSimulator =
                std::make_unique<ExchangeSimulator>(
                        std::move(exchangeBeanFactory),
                        std::move(serviceFactory),
                        std::move(exchangeIdService)
                        );


        std::thread persistStatsthread (&ExchangeSimulator::persistStats, exchangeSimulator.get());
        persistStatsthread.detach ();


        MLOG(3)<<" starting the exchange simulator :  "<<argc<<" , argv : "<<*argv;


        if (StringUtil::equalsIgnoreCase(loadTestActionRecorderProp, "yes")) {
                std::thread actionRecorderRequestsThread1 (&ActionRecorderLoadTester::startLoadTesting,
                                                           exchangeSimulator->actionRecorderLoadTester.get());
                LOG(INFO)<< "actionRecorderRequestsThread1 started with id " <<actionRecorderRequestsThread1.get_id();

                actionRecorderRequestsThread1.detach();
        } else {
                LOG(INFO)<<"not load testing action recorder based on properties set";
        }

        if (StringUtil::equalsIgnoreCase(loadTestAdServerProp, "yes")) {
                int numberOfAdServerLoadTester = 1;
                for (int i=0; i < numberOfAdServerLoadTester; i++) {
                        std::thread loadTestAdServerThread (&AdServerLoadTester::startLoadTestingAdServer,
                                                            exchangeSimulator->adServerLoadTester.get());

                        loadTestAdServerThread.detach();
                }
        } else {
                LOG(INFO)<<"not load testing adserver based on properties set";
        }


        std::thread overallSystemLoadTestingThread (&ExchangeSimulator::startOverallSystemLoadTesting, exchangeSimulator.get());
        overallSystemLoadTestingThread.detach ();

        std::thread adServerCallerThread (&ExchangeSimulator::startAdServerCallerThread, exchangeSimulator.get());
        adServerCallerThread.detach ();


        exchangeSimulator->run(argc, argv);

        LOG(ERROR)<< "END OF PROGRAM";
}
