#include "ExchangeBeanFactory.h"
#include "MySqlMetricService.h"
#include "MySqlCampaignService.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupFilterStatistic.h"
#include "TargetGroupCacheService.h"
#include "MySqlPixelService.h"
#include "MySqlBadDeviceService.h"
#include "CrossWalkedDeviceMap.h"
#include "MySqlPixelClusterResultService.h"
#include "GUtil.h"
#include "ModelScore.h"
#include "OfferCacheService.h"
#include "ModelResultCacheService.h"
#include "MySqlModelResultService.h"
#include "BootableConfigService.h"
#include "WiretapCassandraService.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "MySqlTargetGroupService.h"
#include "PixelCacheService.h"
#include "MySqlClientService.h"
#include "Client.h"
#include "BootableConfigClient.h"
#include "FeatureGuardService.h"
#include "ObjectVectorHolder.h"
#include "MySqlGeoSegmentListService.h"
#include "GeoLocationCacheService.h"
#include "MySqlGeoLocationService.h"
#include "MySqlTargetGroupRecencyModelMapService.h"
#include "LatLonToPolygonConverter.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "ExchangeBeanFactory.h"
#include "NetworkUtil.h"
#include "MySqlSegmentService.h"
#include "MySqlTargetGroupSegmentMapService.h"
#include "MySqlHighPerformanceService.h"
#include "MySqlTargetGroupMgrsSegmentMapService.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "MySqlAdvertiserService.h"
#include "AdInteractionRecordingModule.h"
#include "MetricDbRecord.h"
#include "CrossWalkUpdaterModule.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoLocationService.h"
#include "CrossWalkReaderModule.h"
#include "TgGeoFeatureCollectionMapCacheService.h"
#include "MySqlClientDealService.h"
#include "TargetGroupSegmentCacheService.h"
#include "ClientDealCacheService.h"
#include "DealCacheService.h"
#include "RealTimeEntityCacheService.h"
#include "GlobalWhiteListedModelCacheService.h"
#include "GlobalWhiteListedCacheService.h"
#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"

#include "TargetGroupBWListCacheService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "TargetGroupDayPartCacheService.h"
#include "TargetGroupGeoLocationCacheService.h"
#include "EntityDeliveryInfoCacheService.h"

#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include "CreativeCacheService.h"
#include "MySqlBWListService.h"
#include "TargetGroupBWListCacheService.h"
#include "TargetGroupCreativeCacheService.h"
#include "TargetGroupMgrsSegmentCacheService.h"
#include "SegmentGroupCacheService.h"
#include "SegmentGroupSegmentMapCacheService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "TargetGroupDayPartCacheService.h"
#include "TargetGroupGeoLocationCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "GlobalWhiteListedCacheService.h"
#include "TargetGroupSegmentGroupCacheService.h"
#include "AdHistoryCassandraService.h"
#include "TargetGroupFilterStatistic.h"
#include "EntityToModuleStateStats.h"
#include "MySqlTargetGroupFilterCountDbRecordService.h"
#include "KafkaProducer.h"


#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include "CreativeCacheService.h"
#include "InfluxDbClient.h"
#include "MySqlBWListService.h"
#include "TargetGroupBWListCacheService.h"

#include "TargetGroupCreativeCacheService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "TargetGroupDayPartCacheService.h"
#include "TargetGroupGeoLocationCacheService.h"
#include "SizeBasedLRUCache.h"
#include "MetricEmailSender.h"
#include "TargetGroupSegmentGroupMap.h"

#include "TgGeoFeatureCollectionMapCacheService.h"
#include "ModelCacheService.h"
#include "GlobalWhiteListedCacheService.h"
#include "GicapodsIdToExchangeIdsMapCassandraService.h"
#include "SegmentCacheService.h"
#include "TargetGroupSegmentCacheService.h"
#include "ClientDealCacheService.h"
#include "DealCacheService.h"
#include "GlobalWhiteListedModelCacheService.h"

#include "EntityDeliveryInfoCacheService.h"
#include "MySqlTgPerformanceTrackService.h"

#include "MySqlEntityRealTimeDeliveryInfoService.h"
// #include "MySqlOfferSegmentService.h"
#include "MySqlTgBiddingPerformanceMetricDtoService.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "DeviceIdService.h"

// #include "MySqlOfferSegmentService.h"
#include "AsyncThreadPoolService.h"
#include "LogLevelManager.h"
#include "DateTimeService.h"
#include "MySqlSegmentGroupService.h"
#include "AerospikeDriver.h"
#include "CassandraDriverInterface.h"
#include "TargetGroupTypeDefs.h"
#include "CreativeTypeDefs.h"
#include "CampaignRealTimeInfo.h"
#include "ConfigService.h"
#include "AtomicLong.h"
#include "MySqlDriver.h"
// #include "MySqlOfferService.h"
#include "MySqlPixelService.h"
// #include "OfferSegmentCacheService.h"
#include "CassandraDriverInterface.h"
#include "CassandraServiceQueueManager.h"
#include "EntityToModuleStateStats.h"
#include "TargetGroupCreativeMap.h"
#include "SegmentToDeviceCassandraService.h"
// #include "OfferSegment.h"
#include "Offer.h"
#include "LatLonToMGRSConverter.h"

#include "RealTimeEntityCacheService.h";
#include "DeviceSegmentHistoryCassandraService.h";
#include "FeatureToFeatureHistoryCassandraService.h";
#include "MySqlTopMgrsSegmentService.h";
#include "FeatureDeviceHistoryCassandraService.h";
#include "BidEventLog.h"
#include "AeroCacheService.h"
#include "EventLock.h"
#include "Feature.h";
#include "CassandraService.h"
#include "PixelDeviceHistoryCassandraService.h"
#include "MetricHealthCheckService.h"

#include "GicapodsIdToExchangeIdsMapCassandraService.h"

#include "DeviceIdToIpsMapCassandraService.h"
#include "IpToDeviceIdsMapCassandraService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "ImpressionEventDto.h"
#include "GicapodsIdToExchangeIdsMapCassandraService.h"
#include "ComparatorService.h"
#include "Creative.h"
#include "MySqlDataMover.h"
// #include "CampaignOffer.h"
#include "Pixel.h"
#include "MySqlHourlyCompressor.h"
#include "Creative.h"
#include "DataMasterRequestHandlerFactory.h"
#include "Campaign.h"
#include "ConcurrentHashSet.h"
// #include "CampaignOfferCacheService.h"
// #include "MySqlCampaignOfferService.h"
// #include "MySqlTargetGroupOfferService.h"
// #include "OfferPixelMapCacheService.h"
// #include "OfferPixelMapCacheService.h"
// #include "MySqlOfferPixelMapService.h"
#include "MySqlFeatureUnderReviewService.h"
#include "SegmentGroupSegmentMap.h"
#include "MySqlSegmentGroupSegmentMapService.h"
#include "MySqlTargetGroupSegmentGroupMapService.h"
#include "MySqlFeatureRegistryService.h"
#include "RealTimeFeatureRegistryCacheUpdaterService.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "MySqlRecencyModelService.h"
#include "CassandraDriver.h"
#include "CassandraDriverNoOp.h"
#include "RecencyModelCacheService.h"
#include "AerospikeDriverNoOp.h"
#include "ObjectGlobalMapContainer.h"
#include "HeartBeatRecorderService.h"

ExchangeBeanFactory::ExchangeBeanFactory(std::string appVersion) {
        this->appVersion = appVersion;
        dequeueAsyncTasksIntervalInSeconds = 1;
}

void ExchangeBeanFactory::initializeModules() {

        assertAndThrow(!propertyFileName.empty());
        assertAndThrow(!commonPropertyFileName.empty());
        assertAndThrow(!appName.empty());
        LOG(INFO) << "propertyFileName : "<< propertyFileName;
        LOG(INFO) << "commonPropertyFileName : "<< commonPropertyFileName;

        configService = std::make_unique<gicapods::ConfigService>(propertyFileName,
                                                                  commonPropertyFileName);

        entityToModuleStateStats = EntityToModuleStateStats::getInstance(configService.get());

        heartBeatRecorderService = std::make_shared<HeartBeatRecorderService>(
                entityToModuleStateStats.get(),
                appName
                );
        //we start the heart beat for every app here
        heartBeatRecorderService->startThread();


        //the first property that we load is loadPropertiesFromRemoteServer
        //which determines whether we load other properties from remote or not
        bootableConfigService = std::make_unique<gicapods::BootableConfigService>(commonPropertyFileName);
        bool loadPropertiesFromRemoteServer = configService->getAsBooleanFromString("loadPropertiesFromRemoteServer");

        LogLevelManager::shared_instance()->allLogsAreOn = configService->getAsBooleanFromString("allLogsAreOn");
        LogLevelManager::shared_instance()->allLoggingIsTurendOff = configService->getAsBooleanFromString("allLoggingIsTurendOff");

        gicapods::Util::exitOnErrorConditions = configService->getAsBooleanFromString("exitOnErrorConditions");
        if (loadPropertiesFromRemoteServer) {
                LOG(INFO) << "loading props from remote server : " + configService->get("removePropertiesServerUrl");
                auto client = std::make_unique<gicapods::BootableConfigClient>(
                        configService->get("removePropertiesServerUrl"),
                        entityToModuleStateStats.get()
                        );

                auto
                loadedProps = client->readPropertiesFromRemoteServer(
                        configService->get("appName"),
                        configService->get("appCluster"),
                        configService->get("appHost"));

                configService->clearAllProperties();
                configService->addProperties(loadedProps);
        }

        ObjectGlobalMapContainer::setMaxLimitOfObjects(configService->getAsLong("maxLimitOfObjectsForLeakAlerts"));
        ObjectGlobalMapContainer::setMaxLimitOfMapEntries(configService->getAsLong("maxLimitOfMapEntriesForLeakAlerts"));
        ObjectGlobalMapContainer::checkMemoryLeakUponCreationProperty = configService->getAsBooleanFromString("checkMemoryLeakUponCreationProperty");
        objectGlobalMapContainer = std::make_unique<ObjectGlobalMapContainer>();
        std::thread checkMemoryLeakStatusThread (&ObjectGlobalMapContainer::checkMemoryLeakStatusThread, objectGlobalMapContainer.get());
        checkMemoryLeakStatusThread.detach ();

        dateTimeService = std::make_unique<DateTimeService>();
        DateTimeUtil::init();




        //TODO remove this bean from ExchangeBeanFactory and use getInstance in client classes
        mySqlDriverMango = MySqlDriver::getInstance(configService.get());


        dataMasterUrl = configService->get ("dataMasterUrl");

        LOG(INFO)<<"dataMasterUrl is : "<<dataMasterUrl;
        int poolSize = 1;
        configService->get("mysqlConnectionPoolSize", poolSize);

        mySqlDriverAlpha1 = std::make_unique<MySqlDriver> (
                configService->get("impression_mysqlUrlConnection"),
                configService->get("impression_mysqlUsername"),
                configService->get("impression_mysqlPassword"),
                configService->get("impression_mysqlSchema"),
                poolSize);

        httpUtilService = HttpUtilService::getInstance(configService.get());

        influxDbClient = std::make_unique<InfluxDbClient> (
                entityToModuleStateStats.get(),
                configService.get());

        auto stateDequeueThreadIsRunning = std::make_shared<gicapods::AtomicBoolean>();
        auto stopStateConsumingThread = std::make_shared<gicapods::AtomicBoolean>();
        entityToModuleStateStats->stateDequeueThreadIsRunning = stateDequeueThreadIsRunning;
        entityToModuleStateStats->stopStateConsumingThread = stopStateConsumingThread;

        if (configService->propertyExists("isStateRecordingDisabled")) {
                std::string isStateRecordingDisabledProp =   configService->get("isStateRecordingDisabled");
                if (StringUtil::equalsIgnoreCase(isStateRecordingDisabledProp, "true")) {
                        entityToModuleStateStats->isStateRecordingDisabled->setValue(true);
                }
        }

        std::thread dequeueStatesThread(&EntityToModuleStateStats::dequeueStates,
                                        entityToModuleStateStats.get());
        dequeueStatesThread.detach();

        std::string disableCassandraResult = configService->get("disableCassandra");
        if(!StringUtil::equalsIgnoreCase(disableCassandraResult, "true")) {
                //try catch is put here, only for using valgrind with apps
                cassandraDriver = std::make_unique<CassandraDriver> (configService.get(),
                                                                     entityToModuleStateStats.get());
                int result = cassandraDriver->startCassandraCluster();
                if (result != 0) {
                        throwEx("failed to startCassandraCluster");
                }
        } else {
                LOG(ERROR) << "NOT STARTING CASSANDRA based on property";
                cassandraDriver = std::make_unique<CassandraDriverNoOp> ();
        }


        std::string disableAerospikeResult = configService->get("disableAerospike");
        if(!StringUtil::equalsIgnoreCase(disableAerospikeResult, "true")) {
                std::string aerospikeHost = configService->get("aerospikeHost");
                int aerospikePort;
                configService->get("aerospikePort", aerospikePort);
                aeroSpikeDriver = std::make_unique<AerospikeDriver>(aerospikeHost, aerospikePort, nullptr);
                aeroSpikeDriver->entityToModuleStateStats = entityToModuleStateStats.get();
        } else {
                LOG(ERROR) << "NOT STARTING AerospikeDriver based on property";
                aeroSpikeDriver =  std::make_unique<AerospikeDriverNoOp>();
                aeroSpikeDriver->entityToModuleStateStats = entityToModuleStateStats.get();
        }

        std::string mailhost = "smtp.mail.yahoo.com";
        std::string username = "m_taabodi";
        std::string password = "67000099Aa?12";

        // mySqlTgPerformanceTrackService = std::make_unique<MySqlTgPerformanceTrackService>(mySqlDriverMango.get());

        mySqlMetricService = std::make_unique<MySqlMetricService>(mySqlDriverMango.get());

        // mySqlTargetGroupService = std::make_unique<MySqlTargetGroupService>(mySqlDriverMango.get());




        // NULL_CHECK(httpUtilService.get());
        // targetGroupCacheService =
        //         TargetGroupCacheService::getInstance(configService.get()).get();

        // mySqlEntityRealTimeDeliveryInfoService =
        //         std::make_unique<MySqlEntityRealTimeDeliveryInfoService>(
        //                 mySqlDriverAlpha1.get(),
        //                 mySqlDriverMango.get()
        //                 );

        this->asyncThreadPoolService = std::make_unique<AsyncThreadPoolService>();
        this->asyncThreadPoolService->entityToModuleStateStats = entityToModuleStateStats.get();
        this->asyncThreadPoolService->runTasksInSeperateThread();

        LOG(INFO)<<"appVersion is : "<<appVersion;

        // this->targetGroupToGeoCollectionKeys = std::make_shared<std::unordered_set<std::string> >();
        // this->targetGroupIdsWithGeoCollectionAssignments = std::make_shared<std::unordered_set<int> >();


        stopConsumingThread = std::make_shared<gicapods::AtomicBoolean>(false);
        // bidderIsStopped = std::make_shared<gicapods::AtomicBoolean>(false);
        asyncThreadIsStopped = std::make_shared<gicapods::AtomicBoolean>(false);

        entityToModuleStateStatsPersistenceService =
                std::make_unique<EntityToModuleStateStatsPersistenceService>(
                        entityToModuleStateStats.get(),
                        mySqlMetricService.get(),
                        configService.get(),
                        appName,
                        appVersion
                        );

        entityToModuleStateStatsPersistenceService->stopConsumingThread = stopConsumingThread;
        entityToModuleStateStatsPersistenceService->dequeueThreadIsRunning = dequeueThreadIsRunning;

        //
        // mySqlCampaignService = std::make_unique<MySqlCampaignService>(mySqlDriverMango.get());
        //
        // campaignCacheService = std::make_unique<CampaignCacheService> (
        //         this->mySqlCampaignService.get(),
        //         targetGroupCacheService,
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // campaignEntityProviderService = std::make_unique<EntityProviderService<Campaign> > (
        //         this->mySqlCampaignService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // mySqlAdvertiserService = std::make_unique<MySqlAdvertiserService>(mySqlDriverMango.get());
        //
        // latLonToMGRSConverter = std::make_unique<LatLonToMGRSConverter>(entityToModuleStateStats.get());
        //
        // advertiserCacheService = std::make_unique<CacheService<Advertiser> > (
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // advertiserEntityProviderService = std::make_unique<EntityProviderService<Advertiser> > (
        //         mySqlAdvertiserService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        //
        // mySqlClientService = std::make_unique<MySqlClientService>(mySqlDriverMango.get());
        //
        // clientCacheService = std::make_unique<CacheService<Client> > (
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        // clientEntityProviderService = std::make_unique<EntityProviderService<Client> > (
        //         mySqlClientService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //



        // mySqlTargetGroupOfferService = std::make_unique<MySqlTargetGroupOfferService>(mySqlDriverMango.get());
        // mySqlCampaignOfferService = std::make_unique<MySqlCampaignOfferService>(mySqlDriverMango.get());
        //
        // targetGroupOfferCacheService =
        //         std::make_unique<TargetGroupOfferCacheService>(
        //                 mySqlTargetGroupOfferService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupOfferEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroupOffer> >(
        //                 mySqlTargetGroupOfferService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );


        // targetGroupEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroup> >(
        //                 mySqlTargetGroupService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // campaignOfferCacheService =
        //         std::make_unique<CampaignOfferCacheService >(
        //                 mySqlCampaignOfferService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // campaignOfferEntityProviderService =
        //         std::make_unique<EntityProviderService<CampaignOffer> >(
        //                 mySqlCampaignOfferService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        //
        //
        // mySqlOfferPixelMapService =
        //         std::make_unique<MySqlOfferPixelMapService>(mySqlDriverMango.get());
        //
        // mySqlOfferSegmentService =
        //         std::make_unique<MySqlOfferSegmentService>(mySqlDriverMango.get());
        //
        // mySqlPixelClusterResultService =
        //         std::make_unique<MySqlPixelClusterResultService>(mySqlDriverMango.get());
        //
        // offerPixelMapCacheService =
        //         std::make_unique<OfferPixelMapCacheService >(
        //                 mySqlOfferPixelMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);
        // offerPixelMapEntityProviderService =
        //         std::make_unique<EntityProviderService<OfferPixelMap> >(
        //                 mySqlOfferPixelMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);
        //
        pixelCacheService =
                std::make_unique<PixelCacheService >(
                        mySqlPixelService.get(),
                        httpUtilService.get(),
                        dataMasterUrl,
                        entityToModuleStateStats.get(),
                        appName);
        //
        // pixelEntityProviderService =
        //         std::make_unique<EntityProviderService<Pixel> >(
        //                 mySqlPixelService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);

        mySqlBWListService = std::make_unique<MySqlBWListService>(mySqlDriverMango.get());
        mySqlTargetGroupBWListMapService = std::make_unique<MySqlTargetGroupBWListMapService>(mySqlDriverMango.get());
        mySqlTargetGroupBWListMapService->mySqlBWListService = mySqlBWListService.get();

        realTimeEventLogCacheService =
                std::make_unique<AeroCacheService<EventLog> >();
        realTimeEventLogCacheService->aeroSpikeDriver = aeroSpikeDriver.get();
        realTimeEventLogCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        mySqlPixelService = std::make_unique<MySqlPixelService>(mySqlDriverMango.get());
        mySqlBWEntryService =
                std::make_unique<MySqlBWEntryService>(mySqlDriverMango.get());

        mySqlGlobalWhiteListService = std::make_unique<MySqlGlobalWhiteListService>(mySqlDriverMango.get());


        gicapodsIdToExchangeIdsMapCassandraService =
                std::make_unique<GicapodsIdToExchangeIdsMapCassandraService>(cassandraDriver.get(),
                                                                             httpUtilService.get(),
                                                                             entityToModuleStateStats.get(),

                                                                             asyncThreadPoolService.get(),
                                                                             configService.get());

        // mySqlTargetGroupSegmentMapService =
        //         std::make_unique<MySqlTargetGroupSegmentMapService>(mySqlDriverMango.get());
        //
        //
        //
        // mySqlTgBiddingPerformanceMetricDtoService =
        //         std::make_unique<MySqlTgBiddingPerformanceMetricDtoService>(mySqlDriverMango.get());
        //
        // mySqlCreativeService  = std::make_unique<MySqlCreativeService>(mySqlDriverMango.get());
        //
        // mySqlTopMgrsSegmentService  = std::make_unique<MySqlTopMgrsSegmentService>(mySqlDriverMango.get());
        //
        // creativeCacheService = std::make_unique<CreativeCacheService> (
        //         mySqlCreativeService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        // creativeEntityProviderService = std::make_unique<EntityProviderService<Creative> > (
        //         mySqlCreativeService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // mySqlTgGeoFeatureCollectionMapService =
        //         std::make_unique<MySqlTgGeoFeatureCollectionMapService>(mySqlDriverMango.get());
        // tgGeoFeatureCollectionMapCacheService =
        //         std::make_unique<TgGeoFeatureCollectionMapCacheService>
        //                 (mySqlTgGeoFeatureCollectionMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        // tgGeoFeatureCollectionMapEntityProviderService =
        //         std::make_unique<EntityProviderService<TgGeoFeatureCollectionMap> >
        //                 (mySqlTgGeoFeatureCollectionMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // tgGeoFeatureCollectionMapCacheService->allTgGeoFeatureCollectionKeys = targetGroupToGeoCollectionKeys;
        // tgGeoFeatureCollectionMapCacheService->targetGroupIdsWithGeoCollectionAssignments = targetGroupIdsWithGeoCollectionAssignments;
        //
        // //TODO remove this bean from ExchangeBeanFactory and use getInstance in client classes
        // mySqlModelService = MySqlModelService::getInstance(configService.get());
        // mySqlModelResultService = std::make_unique<MySqlModelResultService>(mySqlDriverMango.get());
        //
        // //TODO remove this bean from ExchangeBeanFactory and use getInstance in client classes
        // mySqlSegmentService = MySqlSegmentService::getInstance(configService.get());
        //
        // segmentCacheService = SegmentCacheService::getInstance(configService.get()).get();
        //
        //
        // segmentEntityProviderService = std::make_unique<EntityProviderService<Segment> >(
        //         mySqlSegmentService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // segmentGroupSegmentMapEntityProviderService =
        //         std::make_shared<EntityProviderService<SegmentGroupSegmentMap> >(
        //                 MySqlSegmentGroupSegmentMapService::getInstance(configService.get()).get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // segmentGroupEntityProviderService = std::make_shared<EntityProviderService<SegmentGroup> >(
        //         MySqlSegmentGroupService::getInstance(configService.get()).get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // deviceIdService = std::make_unique<DeviceIdService>(entityToModuleStateStats.get());
        // deviceIdService->aeroSpikeDriver= aeroSpikeDriver.get();
        //
        //
        // targetGroupSegmentCacheService =
        //         std::make_unique<TargetGroupSegmentCacheService>(
        //                 mySqlTargetGroupSegmentMapService.get(),
        //                 segmentCacheService,
        //                 targetGroupCacheService,
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupSegmentEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroupSegmentMap> >(
        //                 mySqlTargetGroupSegmentMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        // targetGroupSegmentGroupMapEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroupSegmentGroupMap> >(
        //                 MySqlTargetGroupSegmentGroupMapService::getInstance(configService.get()).get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupBWListCacheService =
        //         std::make_unique<TargetGroupBWListCacheService> (
        //                 mySqlTargetGroupBWListMapService.get(),
        //                 targetGroupCacheService,
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupBWListEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroupBWListMap> > (
        //                 mySqlTargetGroupBWListMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // mySqlTargetGroupCreativeMapService =
        //         std::make_unique<MySqlTargetGroupCreativeMapService>(mySqlDriverMango.get());
        // mySqlTargetGroupCreativeMapService->mySqlCreativeService = mySqlCreativeService.get();
        //
        //
        // mySqlTargetGroupMgrsSegmentMapService =
        //         std::make_unique<MySqlTargetGroupMgrsSegmentMapService>(mySqlDriverMango.get());
        // mySqlTargetGroupMgrsSegmentMapService->mySqlTopMgrsSegmentService = mySqlTopMgrsSegmentService.get();
        //
        //
        // targetGroupCreativeCacheService =
        //         std::make_unique<TargetGroupCreativeCacheService>(
        //                 mySqlTargetGroupCreativeMapService.get(),
        //                 targetGroupCacheService,
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupMgrsSegmentCacheService =
        //         std::make_unique<TargetGroupMgrsSegmentCacheService>(
        //                 mySqlTargetGroupMgrsSegmentMapService.get(),
        //                 targetGroupCacheService,
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupCreativeEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroupCreativeMap> >(
        //                 mySqlTargetGroupCreativeMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // targetGroupMgrsSegmentEntityProviderService =
        //         std::make_unique<EntityProviderService<TargetGroupMgrsSegmentMap> >(
        //                 mySqlTargetGroupMgrsSegmentMapService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // mySqlTargetGroupGeoSegmentListMapService =
        //         std::make_unique<MySqlTargetGroupGeoSegmentListMapService>(mySqlDriverMango.get());
        //
        // targetGroupGeoSegmentCacheService = std::make_unique<TargetGroupGeoSegmentCacheService>(
        //         mySqlTargetGroupGeoSegmentListMapService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // mySqlTargetGroupDayPartTargetMapService =
        //         std::make_unique<MySqlTargetGroupDayPartTargetMapService>(mySqlDriverMango.get());
        //
        // targetGroupDayPartCacheService  = std::make_unique<TargetGroupDayPartCacheService>(
        //         mySqlTargetGroupDayPartTargetMapService.get(),
        //         targetGroupCacheService,
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // targetGroupDayPartEntityProviderService  = std::make_unique<EntityProviderService<TargetGroupDayPartTargetMap> >(
        //         mySqlTargetGroupDayPartTargetMapService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // if (configService->propertyExists("scoringKafkaCluster")) {
        //         std::string scoringKafkaCluster = configService->get("scoringKafkaCluster");
        //         std::string scoringTopicName = configService->get("scoringTopicName");
        //         std::string scoringLaneGroupId = configService->get("scoringLaneGroupId");
        //
        //
        //         int32_t partitionNumber = RdKafka::Topic::PARTITION_UA;
        //         //
        //         // /** @brief Special offsets */
        //         // static const int64_t OFFSET_BEGINNING = -2; /**< Consume from beginning */
        //         // static const int64_t OFFSET_END       = -1; /**< Consume from end */
        //         // static const int64_t OFFSET_STORED    = -1000; /**< Use offset storage */
        //         // static const int64_t OFFSET_INVALID   = -1001;
        //         kafkaProducer = std::make_unique<KafkaProducer>(
        //                 scoringKafkaCluster,
        //                 scoringLaneGroupId,
        //                 scoringTopicName,
        //                 partitionNumber);
        //
        //         kafkaProducer->queueSizeWaitingForAck = queueSizeWaitingForAck;
        //         kafkaProducer->entityToModuleStateStats= entityToModuleStateStats.get();
        // } else {
        //         LOG(ERROR)<< "skipping kafkaProducer creation";
        // }
        //
        // mySqlGeoLocationService = std::make_unique<MySqlGeoLocationService>(
        //         mySqlDriverMango.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl);
        //
        // geoLocationCacheService = std::make_unique<GeoLocationCacheService>(
        //         mySqlGeoLocationService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // modelResultCacheService = std::make_unique<ModelResultCacheService>(
        //         mySqlModelResultService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // modelResultEntityProviderService = std::make_unique<EntityProviderService<ModelResult> >(
        //         mySqlModelResultService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // geoLocationEntityProviderService = std::make_unique<EntityProviderService<GeoLocation> >(
        //         mySqlGeoLocationService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // mySqlTargetGroupGeoLocationService =
        //         std::make_unique<MySqlTargetGroupGeoLocationService>(
        //                 mySqlDriverMango.get(),
        //                 mySqlGeoLocationService.get());
        //
        //
        // targetGroupGeoLocationCacheService = std::make_unique<TargetGroupGeoLocationCacheService>(
        //         mySqlTargetGroupGeoLocationService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // targetGroupGeoLocationEntityProviderService = std::make_unique<
        //         EntityProviderService<TargetGroupGeoLocationList> >(
        //         mySqlTargetGroupGeoLocationService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        //
        // entityDeliveryInfoCacheService = std::make_unique<EntityDeliveryInfoCacheService>(
        //         entityToModuleStateStats.get());
        //
        //
        globalWhiteListedCacheService =
                std::make_unique<GlobalWhiteListedCacheService>(
                        mySqlGlobalWhiteListService.get(),
                        httpUtilService.get(),
                        dataMasterUrl,
                        entityToModuleStateStats.get(),
                        appName
                        );
        //
        //
        // globalWhiteListedEntityProviderService =
        //         std::make_unique<EntityProviderService<GlobalWhiteListEntry> >(
        //                 mySqlGlobalWhiteListService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        // globalWhiteListedEntityProviderService->minReloadDataFromDbIntervalInSeconds = 300;
        // mySqlGlobalWhiteListModelService  = std::make_unique<MySqlGlobalWhiteListModelService>(mySqlDriverMango.get());
        //
        // globalWhiteListedModelCacheService = std::make_unique<GlobalWhiteListedModelCacheService>(
        //         mySqlGlobalWhiteListModelService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName);
        //
        // mySqlDealService = std::make_unique<MySqlDealService>(mySqlDriverMango.get());
        // mySqlClientDealService = std::make_unique<MySqlClientDealService>(mySqlDriverMango.get());
        //
        //
        // clientDealCacheService =
        //         std::make_unique<ClientDealCacheService>(
        //                 mySqlClientDealService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // clientDealEntityProviderService =
        //         std::make_unique<EntityProviderService<ClientDeal> >(
        //                 mySqlClientDealService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName
        //                 );
        //
        // dealCacheService =
        //         std::make_unique<DealCacheService>(
        //                 mySqlDealService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);
        // dealEntityProviderService =
        //         std::make_unique<EntityProviderService<Deal> >(
        //                 mySqlDealService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);
        //
        // mySqlBadDeviceService = std::make_unique<MySqlBadDeviceService>(mySqlDriverMango.get());
        //
        // badDeviceCacheService =
        //         std::make_unique<CacheService<BadDevice> >(
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);
        //
        // campaignRealTimeInfoCacheService = std::make_unique<CacheService<CampaignRealTimeInfo> >(
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName);
        //
        //
        // mySqlOfferService = std::make_unique<MySqlOfferService>(mySqlDriverMango.get());
        //
        // pixelEntityProviderService = std::make_unique<EntityProviderService<Pixel> > (
        //         mySqlPixelService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // offerCacheService = std::make_unique<OfferCacheService> (
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // offerEntityProviderService = std::make_unique<EntityProviderService<Offer> > (
        //         mySqlOfferService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // offerSegmentEntityProviderService = std::make_unique<EntityProviderService<OfferSegment> > (
        //         mySqlOfferSegmentService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // offerSegmentCacheService = std::make_unique<OfferSegmentCacheService> (
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        // offerSegmentCacheService->segmentCacheService = segmentCacheService;
        //
        // mySqlAdvertiserModelMappingService
        //         = std::make_unique<MySqlAdvertiserModelMappingService>(mySqlDriverMango.get());
        //
        //
        // modelCacheService = std::make_unique<ModelCacheService>(
        //         mySqlModelService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName);
        //
        // modelCacheService->timeoutForDataCallInMillis = 10000;
        //
        // generalModelEntityProviderService = std::make_unique<EntityProviderService<ModelRequest> >(
        //         mySqlModelService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName);
        //
        //
        // eventLockPersistenceService =
        //         std::make_unique<AeroCacheService<EventLock> >();
        // eventLockPersistenceService->aeroSpikeDriver = aeroSpikeDriver.get();
        // eventLockPersistenceService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        //
        // placesCachedResultAeroCacheService =
        //         std::make_unique<AeroCacheService<PlacesCachedResult> >();
        // placesCachedResultAeroCacheService->aeroSpikeDriver = aeroSpikeDriver.get();
        // placesCachedResultAeroCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        //
        // adhistoryCacheService
        //         = std::make_unique<RealTimeEntityCacheService<AdHistory> >(
        //         configService->getAsInt("adHistoryLocalCacheSize"), entityToModuleStateStats.get());
        //
        // realTimeAdHistoryAerospikeCacheService =
        //         std::make_unique<AeroCacheService<AdHistory> >();
        // realTimeAdHistoryAerospikeCacheService->aeroSpikeDriver = aeroSpikeDriver.get();
        // realTimeAdHistoryAerospikeCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        //
        //
        //
        // adhistoryCacheService->realTimeEntityCassandraService = adHistoryCassandraService.get();
        // adhistoryCacheService->realTimeEntityAerospikeCacheService = realTimeAdHistoryAerospikeCacheService.get();
        // adhistoryCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        //
        // deviceSegmentHistoryCassandraService =
        //         std::make_unique<DeviceSegmentHistoryCassandraService>(
        //                 cassandraDriver.get(),
        //                 httpUtilService.get(),
        //                 entityToModuleStateStats.get(),
        //
        //                 asyncThreadPoolService.get(),
        //                 configService.get());
        // deviceSegmentHistoryCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("DeviceSegmentHistory", deviceSegmentHistoryCassandraService.get()));
        //
        // realTimeDeviceSegementHistoryAerospikeCacheService =
        //         std::make_unique<AeroCacheService<DeviceSegmentHistory> >();
        //
        // realTimeDeviceSegementHistoryAerospikeCacheService->aeroSpikeDriver = aeroSpikeDriver.get();
        // realTimeDeviceSegementHistoryAerospikeCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        // realTimeEntityCacheServiceDeviceSegmentHistory =
        //         std::make_unique<RealTimeEntityCacheService<DeviceSegmentHistory> >(
        //                 configService->getAsInt("deviceSegmentHistoryLocalCacheSize"), entityToModuleStateStats.get());
        // realTimeEntityCacheServiceDeviceSegmentHistory->realTimeEntityCassandraService = deviceSegmentHistoryCassandraService.get();
        // realTimeEntityCacheServiceDeviceSegmentHistory->realTimeEntityAerospikeCacheService = realTimeDeviceSegementHistoryAerospikeCacheService.get();
        // realTimeEntityCacheServiceDeviceSegmentHistory->entityToModuleStateStats = entityToModuleStateStats.get();
        //

        //
        // gicapodsIdToExchangeIdsMapCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("GicapodsIdToExchangeIdsMap", gicapodsIdToExchangeIdsMapCassandraService.get()));
        //
        // eventLogCassandraService =
        //         std::make_unique<CassandraService<EventLog> >(
        //                 cassandraDriver.get(),
        //                 entityToModuleStateStats.get(),
        //                 asyncThreadPoolService.get(),
        //                 configService.get());
        // eventLogCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("EventLog", eventLogCassandraService.get()));
        //
        //
        //
        // bidEventLogCassandraService =
        //         std::make_unique<CassandraService<BidEventLog> >(
        //                 cassandraDriver.get(),
        //                 entityToModuleStateStats.get(),
        //                 asyncThreadPoolService.get(),
        //                 configService.get());
        // bidEventLogCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("BidEventLog", bidEventLogCassandraService.get()));
        //
        //
        // modelScoreCassandraService =
        //         std::make_unique<CassandraService<ModelScore> >(
        //                 cassandraDriver.get(),
        //                 entityToModuleStateStats.get(),
        //                 asyncThreadPoolService.get(),
        //                 configService.get());
        // modelScoreCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("ModelScore", modelScoreCassandraService.get()));
        //
        //
        //
        //
        // deviceFeatureHistoryCassandraService = std::make_unique<DeviceFeatureHistoryCassandraService>(
        //         cassandraDriver.get(),
        //         httpUtilService.get(),
        //         entityToModuleStateStats.get(),
        //
        //         asyncThreadPoolService.get(),
        //         configService.get());
        // deviceFeatureHistoryCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("DeviceFeatureHistory", deviceFeatureHistoryCassandraService.get()));
        //
        //
        // featureDeviceHistoryCassandraService = std::make_unique<FeatureDeviceHistoryCassandraService>(
        //         cassandraDriver.get(),
        //         httpUtilService.get(),
        //         entityToModuleStateStats.get(),
        //
        //         asyncThreadPoolService.get(),
        //         configService.get());
        // featureDeviceHistoryCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("FeatureDeviceHistory", featureDeviceHistoryCassandraService.get()));
        //
        //
        //
        // featureToFeatureHistoryCassandraService = std::make_unique<FeatureToFeatureHistoryCassandraService>(
        //         cassandraDriver.get(),
        //         httpUtilService.get(),
        //         entityToModuleStateStats.get(),
        //
        //         asyncThreadPoolService.get(),
        //         configService.get());
        // featureToFeatureHistoryCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("FeatureToFeatureHistory", featureToFeatureHistoryCassandraService.get()));
        //
        //
        //
        // ipToDeviceIdsMapCassandraService
        //         = std::make_unique<IpToDeviceIdsMapCassandraService>(
        //         cassandraDriver.get(),
        //         httpUtilService.get(),
        //         entityToModuleStateStats.get(),
        //
        //         asyncThreadPoolService.get(),
        //         configService.get());
        // ipToDeviceIdsMapCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("IpToDeviceIdsMap", ipToDeviceIdsMapCassandraService.get()));
        //
        // deviceIdToIpsMapCassandraService
        //         = std::make_unique<DeviceIdToIpsMapCassandraService>(
        //         cassandraDriver.get(),
        //         httpUtilService.get(),
        //         entityToModuleStateStats.get(),
        //
        //         asyncThreadPoolService.get(),
        //         configService.get());
        // deviceIdToIpsMapCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("DeviceIdToIpsMap", deviceIdToIpsMapCassandraService.get()));
        //
        // segmentToDeviceCassandraService =
        //         std::make_unique<SegmentToDeviceCassandraService>(
        //                 cassandraDriver.get(),
        //                 httpUtilService.get(),
        //                 entityToModuleStateStats.get(),
        //                 asyncThreadPoolService.get(),
        //                 configService.get());
        // segmentToDeviceCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("SegmentDevices", segmentToDeviceCassandraService.get()));
        //
        // pixelDeviceHistoryCassandraService =
        //         std::make_unique<PixelDeviceHistoryCassandraService>(cassandraDriver.get(),
        //                                                              httpUtilService.get(),
        //                                                              entityToModuleStateStats.get(),
        //
        //                                                              asyncThreadPoolService.get(),
        //                                                              configService.get());
        // pixelDeviceHistoryCassandraService->cassandraServiceQueueManager = cassandraServiceQueueManager.get();
        // cassandraServiceQueueManager->mapOfTypesToCassandraServices->insert(
        //         std::make_pair("PixelDeviceHistory", pixelDeviceHistoryCassandraService.get()));
        //

        //
        //
        //
        //

        //
        //
        // mySqlImpressionMover =
        //         std::make_unique<MySqlDataMover<ImpressionEventDto> >(
        //                 mySqlDriverAlpha1.get());
        // mySqlMetricCompressor =
        //         std::make_unique<MySqlHourlyCompressor<MetricDbRecord> >(
        //                 mySqlDriverMango.get(),
        //                 mySqlDriverMango.get());
        //
        // adInteractionRecordingModule =
        //         std::make_unique<AdInteractionRecordingModule>(
        //                 entityToModuleStateStats.get(),
        //                 eventLogCassandraService.get(),
        //                 NetworkUtil::getHostName(),
        //                 appVersion);
        //
        // adInteractionRecordingModule->influxDbClient = influxDbClient.get();
        //
        // crossWalkUpdaterModule =
        //         std::make_unique<CrossWalkUpdaterModule>();
        // crossWalkUpdaterModule->ipToDeviceIdsMapCassandraService = ipToDeviceIdsMapCassandraService.get();
        // crossWalkUpdaterModule->deviceIdToIpsMapCassandraService = deviceIdToIpsMapCassandraService.get();
        // crossWalkUpdaterModule->entityToModuleStateStats= entityToModuleStateStats.get();
        //
        //
        // crossWalkedDeviceMapAerospikeCacheService =
        //         std::make_unique<AeroCacheService<CrossWalkedDeviceMap> >();
        //
        // crossWalkedDeviceMapAerospikeCacheService->aeroSpikeDriver = aeroSpikeDriver.get();
        // crossWalkedDeviceMapAerospikeCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        // mySqlRecencyModelService = std::make_unique<MySqlRecencyModelService>(mySqlDriverMango.get());
        //
        //
        // mySqlTargetGroupRecencyModelMapService =
        //         std::make_unique<MySqlTargetGroupRecencyModelMapService>(
        //                 mySqlDriverMango.get(),
        //                 mySqlRecencyModelService.get());
        //
        // targetGroupRecencyModelMapCacheService = std::make_unique<TargetGroupRecencyModelMapCacheService>(
        //         mySqlTargetGroupRecencyModelMapService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        // targetGroupRecencyModelMapEntityProviderService = std::make_unique<EntityProviderService<TargetGroupRecencyModelMap> >(
        //         mySqlTargetGroupRecencyModelMapService.get(),
        //         httpUtilService.get(),
        //         dataMasterUrl,
        //         entityToModuleStateStats.get(),
        //         appName
        //         );
        //
        // crossWalkReaderModule = std::make_unique<CrossWalkReaderModule>();
        //
        // crossWalkReaderModule->ipToDeviceIdsMapCassandraService = ipToDeviceIdsMapCassandraService.get();
        // crossWalkReaderModule->deviceIdToIpsMapCassandraService = deviceIdToIpsMapCassandraService.get();
        // crossWalkReaderModule->entityToModuleStateStats= entityToModuleStateStats.get();
        // crossWalkReaderModule->crossWalkedDeviceMapAerospikeCacheService= crossWalkedDeviceMapAerospikeCacheService.get();
        //
        //
        //
        // mySqlTargetGroupFilterCountDbRecordService =
        //         std::make_unique<MySqlTargetGroupFilterCountDbRecordService>(
        //                 mySqlDriverMango.get(),
        //                 NetworkUtil::getHostName(),
        //                 appVersion
        //                 );
        //
        //
        // featuresAerospikeCacheService =
        //         std::make_unique<AeroCacheService<Feature> >();
        // featuresAerospikeCacheService->aeroSpikeDriver = aeroSpikeDriver.get();
        // featuresAerospikeCacheService->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        //
        // mySqlFeatureUnderReviewService =
        //         std::make_unique<MySqlFeatureUnderReviewService>(
        //                 mySqlDriverAlpha1.get(),
        //                 entityToModuleStateStats.get());
        //
        // mySqlFeatureRegistryService =
        //         std::make_unique<MySqlFeatureRegistryService>(
        //                 mySqlDriverAlpha1.get(),
        //                 entityToModuleStateStats.get());
        //
        //
        //
        // realTimeFeatureRegistryCacheUpdaterService = std::make_unique<RealTimeFeatureRegistryCacheUpdaterService>();
        //
        // realTimeFeatureRegistryCacheUpdaterService->mySqlFeatureUnderReviewService = mySqlFeatureUnderReviewService.get();
        // realTimeFeatureRegistryCacheUpdaterService->mySqlFeatureRegistryService = mySqlFeatureRegistryService.get();
        // realTimeFeatureRegistryCacheUpdaterService->entityToModuleStateStats = entityToModuleStateStats.get();
        // realTimeFeatureRegistryCacheUpdaterService->featuresAerospikeCacheService = featuresAerospikeCacheService.get();
        //
        //
        //
        // mySqlHighPerformanceFeatureUnderReviewService =
        //         std::make_shared<MySqlHighPerformanceService<std::string, Feature> >(
        //                 mySqlDriverAlpha1.get(),
        //                 entityToModuleStateStats.get(),
        //                 20
        //                 );
        //
        // mySqlHighPerformanceFeatureUnderReviewService->mySqlService = mySqlFeatureUnderReviewService.get();
        //
        //
        // auto featuresCache =
        //         std::make_shared<gicapods::SizeBasedLRUCache<std::string, std::shared_ptr<Feature> > > (
        //                 configService->getAsInt("featureGuardServiceFeatureCacheSize"), "Feature");
        // featuresCache->entityToModuleStateStats = entityToModuleStateStats.get();
        //
        // featureGuardService = std::make_unique<FeatureGuardService>();
        // featureGuardService->mySqlFeatureUnderReviewService = mySqlFeatureUnderReviewService.get();
        // featureGuardService->mySqlFeatureRegistryService = mySqlFeatureRegistryService.get();
        // featureGuardService->mySqlHighPerformanceFeatureUnderReviewService = mySqlHighPerformanceFeatureUnderReviewService.get();
        // featureGuardService->entityToModuleStateStats = entityToModuleStateStats.get();
        // featureGuardService->featuresAerospikeCacheService = featuresAerospikeCacheService.get();
        // featureGuardService->featuresCache = featuresCache;
        //
        //
        // this->recencyModelCacheService =
        //         std::make_unique<RecencyModelCacheService>(
        //                 mySqlRecencyModelService.get(),
        //                 httpUtilService.get(),
        //                 dataMasterUrl,
        //                 entityToModuleStateStats.get(),
        //                 appName);
        //
        // latLonToPolygonConverter = std::make_unique<LatLonToPolygonConverter>(entityToModuleStateStats.get());


        addCachesToList();
}



void ExchangeBeanFactory::addCachesToList() {

        // allCacheServices.push_back(modelResultCacheService.get());
        // allCacheServices.push_back(geoLocationCacheService.get());
        // allCacheServices.push_back(clientCacheService.get());
        // allCacheServices.push_back(advertiserCacheService.get());
        // allCacheServices.push_back(creativeCacheService.get());
        // allCacheServices.push_back(campaignCacheService.get());
        // allCacheServices.push_back(targetGroupCacheService);
        // allCacheServices.push_back(SegmentGroupCacheService::getInstance(configService.get()).get());
        // allCacheServices.push_back(SegmentGroupSegmentMapCacheService::getInstance(configService.get()).get());
        // allCacheServices.push_back(segmentCacheService);
        // allCacheServices.push_back(TargetGroupSegmentGroupCacheService::getInstance(configService.get()).get());
        allCacheServices.push_back(targetGroupBWListCacheService.get());
        // allCacheServices.push_back(targetGroupCreativeCacheService.get());
        // allCacheServices.push_back(targetGroupMgrsSegmentCacheService.get());
        // allCacheServices.push_back(targetGroupDayPartCacheService.get());
        // allCacheServices.push_back(targetGroupGeoLocationCacheService.get());
        allCacheServices.push_back(globalWhiteListedCacheService.get());
        // allCacheServices.push_back(tgGeoFeatureCollectionMapCacheService.get());
        // allCacheServices.push_back(modelCacheService.get());
        // allCacheServices.push_back(targetGroupSegmentCacheService.get());
        allCacheServices.push_back((CacheService<Offer>*)offerCacheService.get());
        allCacheServices.push_back((CacheService<Pixel>*)pixelCacheService.get());

        allCacheServices.push_back(pixelCacheService.get());
        // allCacheServices.push_back(dealCacheService.get());
        // allCacheServices.push_back(clientDealCacheService.get());
        // allCacheServices.push_back(offerPixelMapCacheService.get());
        // allCacheServices.push_back(campaignOfferCacheService.get());
        // allCacheServices.push_back(targetGroupOfferCacheService.get());
        // allCacheServices.push_back(targetGroupRecencyModelMapCacheService.get());

        allEntityProviderService.push_back(globalWhiteListedEntityProviderService.get());
        allEntityProviderService.push_back(targetGroupBWListEntityProviderService.get());

        // allEntityProviderService.push_back(modelResultEntityProviderService.get());
        // allEntityProviderService.push_back(offerSegmentEntityProviderService.get());
        // allEntityProviderService.push_back(geoLocationEntityProviderService.get());
        // allEntityProviderService.push_back(clientEntityProviderService.get());
        // allEntityProviderService.push_back(advertiserEntityProviderService.get());
        // allEntityProviderService.push_back(creativeEntityProviderService.get());
        // allEntityProviderService.push_back(campaignEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupEntityProviderService.get());
        // allEntityProviderService.push_back(segmentEntityProviderService.get());
        // allEntityProviderService.push_back(segmentGroupSegmentMapEntityProviderService.get());
        // allEntityProviderService.push_back(segmentGroupEntityProviderService.get());

        // allEntityProviderService.push_back(targetGroupCreativeEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupMgrsSegmentEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupDayPartEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupGeoLocationEntityProviderService.get());
        //
        // allEntityProviderService.push_back(tgGeoFeatureCollectionMapEntityProviderService.get());
        // allEntityProviderService.push_back(generalModelEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupSegmentEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupSegmentGroupMapEntityProviderService.get());
        // allEntityProviderService.push_back(offerEntityProviderService.get());
        // allEntityProviderService.push_back(pixelEntityProviderService.get());
        // allEntityProviderService.push_back(dealEntityProviderService.get());
        // allEntityProviderService.push_back(clientDealEntityProviderService.get());
        // allEntityProviderService.push_back(offerPixelMapEntityProviderService.get());
        // allEntityProviderService.push_back(campaignOfferEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupOfferEntityProviderService.get());
        // allEntityProviderService.push_back(targetGroupRecencyModelMapEntityProviderService.get());
}

ExchangeBeanFactory::~ExchangeBeanFactory() {

}
