#ifndef ExchangeSimulator_H
#define ExchangeSimulator_H




#include <memory>
#include <string>
#include <vector>
#include <set>

class MySqlDriver;
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class Creative;

#include "AtomicLong.h"

class MySqlPixelService;
class AdServerLoadTester;
class TargetGroupBWListCacheService;
class EntityToModuleStateStats;
class EntityToModuleStateStatsPersistenceService;
class BidderLoadTester;
class GicapodsIdToExchangeIdsMapCassandraService;
class MySqlCreativeService;
class HttpUtilService;
class MySqlGlobalWhiteListService;
class BidRequestEnricher;
class ExchangeIdService;
class ExchangeBeanFactory;
class BidderLoadTester;
class AdServerCaller;
class ExchangeSimulator;
class OverallSystemLoadTester;
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "ConfigService.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Net/ServerSocket.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServerParams;
using Poco::Net::ServerSocket;

using Poco::Util::Option;
using Poco::Util::OptionSet;
#include <boost/exception/all.hpp>
#include "ConverterUtil.h"
#include "ExchangeSimulatorRequestHandlerFactory.h"
#include <thread>
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include <boost/exception/all.hpp>
#include "PocoSession.h"
#include "AdServerLoadTester.h"
#include "ActionRecorderLoadTester.h"
#include "ServiceFactory.h"
#include "ConcurrentQueueFolly.h"
#include "ConfigService.h"
#include "DataReloadService.h"

class ExchangeSimulator : public std::enable_shared_from_this<ExchangeSimulator>, public Poco::Util::ServerApplication {

public:
std::unique_ptr<BidRequestEnricher> bidRequestEnricher;
std::unique_ptr<ExchangeBeanFactory> exchangeBeanFactory;
std::unique_ptr<ServiceFactory> serviceFactory;
EntityToModuleStateStats* entityToModuleStateStats;
DataReloadService* dataReloadService;
gicapods::ConfigService* configService;
std::shared_ptr<ExchangeSimulatorRequestHandlerFactory> exchangeSimulatorRequestHandlerFactory;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
std::shared_ptr<ConcurrentQueueFolly<std::string> > bidderResponses;
std::unique_ptr<BidderLoadTester> bidderLoadTester;
std::unique_ptr<ExchangeIdService> exchangeIdService;
std::shared_ptr<ActionRecorderLoadTester> actionRecorderLoadTester;
std::shared_ptr<AdServerLoadTester> adServerLoadTester;


ExchangeSimulator(
        std::unique_ptr<ExchangeBeanFactory> exchangeBeanFactory,
        std::unique_ptr<ServiceFactory> serviceFactory,
        std::unique_ptr<ExchangeIdService> exchangeIdService);

void insertRandomGlobalWhiteListDomains();
int insertBwList(std::string uniqueName, std::string listType, int advertiserId);
std::vector<std::shared_ptr<Creative> > insertCreatives(MySqlCreativeService* mySqlCreativeService,
                                                        std::string uniqueName,
                                                        int advertiserId);

std::shared_ptr<OverallSystemLoadTester> createOverallLoadTester();
std::shared_ptr<AdServerCaller> createAdServerCaller();
void startOverallSystemLoadTesting();
void startAdServerCallerThread();
void persistStats();
int main(const std::vector<std::string>& args);
virtual ~ExchangeSimulator();
};

#endif
