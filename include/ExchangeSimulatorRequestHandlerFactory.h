#ifndef ExchangeSimulatorRequestHandlerFactory_H
#define ExchangeSimulatorRequestHandlerFactory_H

#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "AtomicBoolean.h"

class EntityToModuleStateStats;
class CommonRequestHandlerFactory;
#include <memory>

class ExchangeSimulatorRequestHandlerFactory;


class ExchangeSimulatorRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

public:
EntityToModuleStateStats* entityToModuleStateStats;
CommonRequestHandlerFactory* commonRequestHandlerFactory;
ExchangeSimulatorRequestHandlerFactory(EntityToModuleStateStats* entityToModuleStateStats);

Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);
};

#endif
